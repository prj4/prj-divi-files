import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  token: any;
  currentUser: any;

  constructor(private adalSvc: MsAdalAngular6Service, private router: Router) {
    this.adalSvc.acquireToken('https://login.microsoftonline.com/').subscribe((resToken: string) => {
      sessionStorage.setItem('currentUser', resToken);

    });
   
  }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = sessionStorage.getItem('currentUser');


    //// add authorization header with jwt token if available
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token,
          // 'Access-Control-Allow-Origin': '*',
          // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
          // 'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
          // 'Access-Control-Allow-Credentials': "true"

        }
      });


    }

    return next.handle(request)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            sessionStorage.removeItem('currentUser');
            sessionStorage.clear();
            this.adalSvc.logout();
            this.router.navigate([''], { queryParams: { returnUrl: request.url } });
            return of(err as any);
          }
          return throwError(err);
        })
      );
  }
}
