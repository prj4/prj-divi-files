export class Pathologistdata {
  pathinstution: Number;
  pathname: Number;
  otherinstiname: String;
  othername: String;
  phone: String;
  email: String;
  fax: String;


}

export class ReviewDetails {
  cellcontentoptone: String;
  cellcontentopttwo: String;
  specfeaturesother: String;
}
