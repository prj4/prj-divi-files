export class Sampledata {
  patname: String;
  patdob: any;
  patgender: String;
  pataddress: String;
  patcity: String;
  patstate: String;
  patzcode: String;
  patcountry: String;
  patphone: String;
  patmrecord: String;

  phyname: Number;
  phyinstution: Number;

  specimenID: String;
  specimenpanel: any;
  datetimecollected: any;
  diagnosis: String;
  specimensite: String;
  pspecimensite: String;
  daterfromstorage: any;

  billingcname: String;
  billingmemberid: String;
  billinggroup: String;
  billinginsuredname: String;
  billingcity: String;
  billingstate: String;
  billingzcode: String;
  billingdob: any;
  billinginsstudyname: Number;
  billingaddressstudy: String;
  billingpstatus: String;
  billingphone: String;
  patstatus: String;
  relatopatient: String;
  paymentmode: String;
  dischargedate: any;

}

