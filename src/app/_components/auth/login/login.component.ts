import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { AuthenticationService } from '../../../_services/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Observable, of, pipe, throwError } from 'rxjs';
import { map, switchMap, debounceTime, catchError } from 'rxjs/operators';
import { SampleService } from '../../../_services/sample.service';
import { first } from 'rxjs/operators';
import {MatFormFieldModule} from '@angular/material/form-field';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public authtoken: any;

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  constructor(private http: HttpClient, private adalSvc: MsAdalAngular6Service, private _SampleService: SampleService, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<any>(sessionStorage.getItem('currentUser'));
    this.currentUser = this.currentUserSubject.asObservable();



    // this.adalSvc.acquireToken('https://login.microsoftonline.com/').subscribe((resToken: string) => {

    //   console.log(resToken);

    //   if (resToken) {
    //     // store user details and jwt token in local storage to keep user logged in between page refreshes
    //     sessionStorage.setItem('currentUser', resToken);
    //     sessionStorage.setItem('samplelistsession', 'nolist');
    //     this.currentUserSubject.next(resToken);
    //     this.router.navigate(['/Jacksonlab/dashboard']);
    //     this._SampleService.logtime()
    //       .pipe(first())
    //       .subscribe((res: any) => {
    //         if (res) {
    //           console.log(res);
    //         }
    //       });

    //   }
    // }),

    //   catchError(err => {

    //     alert('Invalid Credentials');
    //     return throwError(err);
    //   })
  }
  ngOnInit() {
  }

}




