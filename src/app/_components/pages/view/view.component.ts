import { Component, OnInit } from '@angular/core';
import { SampleService } from '../../../_services/sample.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  loading = false;
  trans_bill: any;
  signbtn: any;
  issigned: any;
  editbtn: any;
  trans_pat: any;
  trans_phy: any;
  trans_speci: any;
  trans_assignpath: any;
  trans_pathreview: any;
  trans_speciFeatures = [];
  trans_speciPanel = [];
  sampleId: string;
  studentObj: any;
  iconchange: string;
  trans_pathreview_spfetures: any;
  currentrole: string;
  patientconhide: boolean;
  phyconhide: boolean;
  specconhide: boolean;
  billconhide: boolean;
  pathrevconhide: boolean;
  messagebox: boolean;
  msgbox: boolean;
  sampID: number;
  pdfname: any;
  dis_date: any;
  param1: any;
  trans_date: any;
  messagecontent: string;
  signresult: any;
  constructor(private _SampleService: SampleService, private adalSvc: MsAdalAngular6Service,
    private _Activatedroute: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer) {
    this.messagebox = false;
    this.msgbox = false;
    this.currentrole = this.adalSvc.userInfo.profile.roles[0];
    this.sampleId = this._Activatedroute.snapshot.params.sampleId;
   // this.param1 = this._Activatedroute.snapshot.queryParams["sample"];
  }

  ngOnInit() {

    this._Activatedroute.queryParams.subscribe(params => {
      this.signresult = params['event'];
      if (this.signresult === "signing_complete") {
        this.msgbox = true;
        this._SampleService.signresponse(this.sampleId, this.signresult)
          .pipe(first())
          .subscribe((res: any) => {
            if (res) {
              this.router.navigate(['/Jacksonlab/viewsample', this.sampleId]);
            }
          });
        this.messagecontent = 'Successfully Signed';
      } else if (this.signresult === "undefined") {
        this.messagebox = false;
      } else if (this.signresult === "cancel" || this.signresult === "decline") {
        this.messagebox = true;
        this.messagecontent = 'Not Signed';
      }
    });

    this._SampleService.getuser(this.sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          this.sampID = res.responseMessage.sampleId;
          this.issigned = res.responseMessage.isSigned;
          this.signbtn = res.responseMessage.isSubmittedToOnchologist;
          this.editbtn = res.responseMessage.isEditable;
          this.trans_bill = res.responseMessage.bill;
          this.dis_date = res.responseMessage.bill.patientStatusId;
          this.trans_pat = res.responseMessage.patient;
          this.trans_date = new Date(Date.parse(res.responseMessage.patient.dob));
          this.trans_phy = res.responseMessage.physician;
          this.trans_speci = res.responseMessage.specimen;
          this.trans_assignpath = res.responseMessage.assignedPathologist;
          this.trans_pathreview = res.responseMessage.pathologistReview;
          this.pdfname = res.responseMessage.pathologistReview.fileName;
          for (let j = 0; j < res.responseMessage.pathologistReview.specimenFeature.length; j++) {
            const specimenf = res.responseMessage.pathologistReview.specimenFeature[j];

            if (specimenf.isSelected === true) {
              const spfdata = {
                'specimenId': specimenf.specimenId,
                'name': specimenf.name,
                'otherComment': specimenf.otherComment,
                'isSelected': specimenf.isSelected
              };
              this.trans_speciFeatures.push(spfdata);
            }

          }
          this.trans_pathreview_spfetures = res.responseMessage.pathologistReview.specimenFeature;
          for (let i = 0; i < res.responseMessage.specimenPanel.length; i++) {
            const speeci = res.responseMessage.specimenPanel[i];

            if (speeci.isSelected === true) {
              const spdata = {
                'specimenId': speeci.specimenId,
                'panelId': speeci.panelId,
                'panelName': speeci.panelName,
                'isSelected': speeci.isSelected
              };
              this.trans_speciPanel.push(spdata);
            }

          }
        } else {
          console.log('error');
        }
      });
  }

  closemessagebox() {
    this.messagebox = false;
    this.msgbox = false;
    if (this.currentrole != 'Pathologist') {
      this.router.navigate(['/Jacksonlab/viewsample',this.sampleId]);
    }
  }

  printt(sampID) {
    this.router.navigate(['/Jacksonlab/print', sampID]);
    window.print();
  }

  printdownload() {
    this.loading = true;
    this._SampleService.printsample(this.sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          const newBlob = new Blob([res], { type: 'application/pdf' });
          const data = window.URL.createObjectURL(res);
          // window.open(data);
          const file_path = data;
          const down = document.createElement('A') as HTMLAnchorElement;
          down.href = file_path;
          down.download = "Sample-".concat(this.sampleId);
          document.body.appendChild(down);
          down.click();
          document.body.removeChild(down);
        }
      });
  }

  downloadreview(filename) {
    this.loading = true;
    this._SampleService.downloadreview(this.sampleId,filename.fileName)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          const newBlob = new Blob([res], { type: 'application/pdf' });
          const data = window.URL.createObjectURL(res);
          // window.open(data);
          const file_path = data;
          const down = document.createElement('A') as HTMLAnchorElement;
          down.href = file_path;
          down.download = filename.displayName;
          document.body.appendChild(down);
          down.click();
          document.body.removeChild(down);
        }
      });
  }
  signapproval() {
    this.loading = true;
    this._SampleService.digitalsign(this.sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          let signurl = res.responseMessage;
          window.location.href = (signurl);
        }
      });
  }

  studysignapproval() {
    this.loading = true;
    this._SampleService.studyemail(this.sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          console.log(res);
          this.loading = false;
          this.messagebox = true;
          this.ngOnInit();
          this.messagecontent = 'Email Initiated Successfully To Sign';
         
        }
      })
  }

}
