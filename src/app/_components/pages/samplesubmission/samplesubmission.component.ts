import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';

import { SampleService } from '../../../_services/sample.service';

declare var $: any;


@Component({
  selector: 'app-samplesubmission',
  templateUrl: './samplesubmission.component.html',
  styleUrls: ['./samplesubmission.component.css']
})
export class SamplesubmissionComponent implements OnInit {

  value: string;
  viewValue: string;
  loading = false; isLinear = false;
  patientSubmission: FormGroup;
  physicianSubmission: FormGroup;
  specimenSubmission: FormGroup;
  billinginsureSubmission: FormGroup;
  billinginstiSubmission: FormGroup;
  billingSubmission: FormGroup;
  billingformselect: FormGroup;
  pathologistSelection: FormGroup;
  specimenlist: [];
  institutions: [];
  doctors: [];
  pathologistsites: [];
  billinstitute: [];
  pathologists: [];
  pathologistdet: [];
  doctorsdet: [];
  trans_speciPanel = [];
  spepanel: any[] = [];
  radiolist;
  realtpatient;
  relpat: any;
  paymode;
  gen: '';
  gender: string;
  inss: number;
  pstate: any;
  submissiondetails: any;
  sampleId: number;
  transactions: any;
  id: any;
  name: string;
  previewvalues: any;
  noneselfpay: any;
  dateToPass: any;
  conmessage: string;
  physicianInstiId: number;
  panellistlength: number;
  spformcheckbox: boolean;
  showtextbox: boolean;
  manmessage: string;
  pathsampleId: number;
  returnsample: number;
  messagebox: boolean;
  messagecontent: string;
  institutionID: number;
  physicianId: number;
  pathnameid: any;
  pathinstid: any;
  phyInsId: number;
  currentrole: string;
  buttonDisabled: boolean;
  pathologyname: string;
  patholoinsti: string;
  showtext: boolean;
  hide3: any;
  hide1: any;
  hide2: any;
  hide: any;
  hide5: any;
  othervalidshow: boolean;
  othervalidemail: boolean;
  dateformatev: any;
  showtextinsti: boolean;
  insti: boolean;
  insur: boolean;
  self_pay: boolean;
  checked: boolean;
  disablebtn: boolean;
  dobmaxDate = new Date();
  datecollmaxDate = new Date();
  loader: boolean;
  otherinstivalidshow: boolean;
  isError: boolean;
  assignpatherror: string;
  assignpathmsg: boolean;
  otherinstifax: boolean;
  ReadOnly:boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _SampleService: SampleService,
    private router: Router,
    private _Activatedroute: ActivatedRoute,
    private adalSvc: MsAdalAngular6Service,
    private _snackBar: MatSnackBar,
    
  ) {
    this.sampleId = this._Activatedroute.snapshot.params.sampleId;
    this.insti = true;
    this.insur = false;
    this.self_pay = false;
    this.checked = true;
    this.disablebtn = true;
    this.messagebox = false;
    this.showtextbox = false;
    this.buttonDisabled = false;
    this.showtext = false;
    this.othervalidshow = false;
    this.othervalidemail = false;
    this.showtextinsti = false;
    this.otherinstivalidshow = false;
    this.currentrole = this.adalSvc.userInfo.profile.roles[0];
    this.isError = false;
    this.assignpathmsg = false;
    this.otherinstifax = false;
    this.ReadOnly = false;
  }


  @ViewChild('stepper') stepper: MatStepper;

  ngOnInit() {

    sessionStorage.setItem('samplelistsessions', 'nolist');
    sessionStorage.setItem('samplelistsession', 'nolist');

    const currentrole = this.adalSvc.userInfo.profile.roles[0];
    if (currentrole === 'Pathologist') {
      this.router.navigate(['/Jacksonlab/dashboard']);
    }
    sessionStorage.removeItem('SampleId');

    this.patientSubmission = this._formBuilder.group({
      patname: ['', Validators.required],
      patdob: ['', Validators.required],
      patgender: ['Male', Validators.required],
      pataddress: [''],
      patcity: [''],
      patstate: [''],
      patzcode: [''],
      patcountry: [''],
      patphone: ['', Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/)],
      patmrecord: ['', Validators.required]
    });
    this.physicianSubmission = this._formBuilder.group({
      phyinstution: ['', Validators.required],
      phyname: ['', Validators.required],
      phyNPI: [''],
      phyaddress: [''],
      phycity: [''],
      phystate: [''],
      phyzcode: [''],
      phycountry: [''],
      phyphone: [''],
      phyemail: [''],
      phyfax: ['']
    });
    this.specimenSubmission = this._formBuilder.group({
      specimenID: [''],
      specimenpanel: [''],
      datetimecollected: [''],
      diagnosis: [''],
      specimensite: [''],
      pspecimensite: [''],
      daterfromstorage: ['']
    });
    this.billinginsureSubmission = this._formBuilder.group({
      billingcname: ['', Validators.required],
      billingmemberid: ['', Validators.required],
      billinggroup: ['', Validators.required],
      billinginsuredname: ['', Validators.required],
      billingcity: ['', Validators.required],
      billingstate: ['', Validators.required],
      billingzcode: ['', Validators.required],
      billingdob: ['', Validators.required],
      billingphone: ['', [Validators.required, Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/)]],
      relatopatient: ['self', Validators.required],
      patstatus: ['out_patient', Validators.required],
      dischargedate:['']
    });

    this.billinginstiSubmission = this._formBuilder.group({
      billinginsstudyname: ['', Validators.required],
      billingaddressstudy: ['', Validators.required]
    });
    this.billingSubmission = this._formBuilder.group({
      paymentmode: ['institution_or_study']
    });

    this.pathologistSelection = this._formBuilder.group({
      pathinstution: ['', Validators.required],
      otherinstiname: [''],
      pathname: ['', Validators.required],
      othername: [''],
      phone: ['', Validators.pattern],
      email: ['', [Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      fax: [''],
    });

      
    

    this._SampleService.specimenpanel()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.specimenlist = res.responseMessage;
          const splen = this.specimenlist.length;
          this.panellistlength = splen - 1;
        } else {
          console.log('error');
        }
      });

// Get Pathologist Institution
    this._SampleService.getpathologistinstitution()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologistsites = res.responseMessage;
        } else {
          console.log('error');
        }
      });

    // Get Billing Institution
    this._SampleService.getbillinstitute()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.billinstitute = res.responseMessage;
        }
        else {
          console.log('error');
        }
      })

// Get Physician Institution
    this._SampleService.getinstitution()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.institutions = res.responseMessage;
          if (this.currentrole === 'Oncologist') {
            this.physicianSubmission.get('phyname').setValue(res.responseMessage[0].physicianId);
            this.physicianSubmission.get('phyNPI').setValue(res.responseMessage[0].physicianNpi);
            this.physicianSubmission.get('phyemail').setValue(res.responseMessage[0].physicianEmail);
          }
          this.physicianId = res.responseMessage.length !== 0 ? res.responseMessage[0].physicianId : 0;
          this.physicianInstiId = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
        } else {
          console.log('error');
        }
      });

    this.spepanel = [1,2]; 
  }





  datec() {
    this.disablebtn = false;
    if (this.specimenSubmission.valid == true) {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else if (this.specimenSubmission.value.datetimecollected === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
    else if (this.specimenSubmission.value.datetimecollected >= new Date()) {
    }
    else {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      }
      else {
        this.disablebtn = false;
      }
      this.disablebtn = false;
    }
  }

  dated() {
    if (this.billinginsureSubmission.value.dischargedate === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
  }


  dates() {
    this.disablebtn = false;
    if (this.specimenSubmission.valid == true) {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else if (this.specimenSubmission.value.daterfromstorage === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
    else if (this.specimenSubmission.value.daterfromstorage >= new Date()) {
    }
    else {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      }
      else {
        this.disablebtn = false;
      }
      this.disablebtn = false;
    }
  }

  // Specimen Validation
  specimenvalidation(stepper) {
    if (this.specimenSubmission.valid && this.spepanel.length > 0) {
      this.spformcheckbox = false;
      this.disablebtn = true;
      stepper.next();

    } else {
      this.spformcheckbox = true;
      this.disablebtn = false;
    }
  }

// Select Physician
  selectdoctor(selectdins) {
    this._SampleService.getdoctors(this.phyInsId, selectdins )
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.doctors = res.responseMessage;
          this.physicianSubmission.get('phyNPI').setValue(res.responseMessage[0].npi);
          this.physicianSubmission.get('phyaddress').setValue(res.responseMessage[0].address);
          this.physicianSubmission.get('phycity').setValue(res.responseMessage[0].city);
          this.physicianSubmission.get('phystate').setValue(res.responseMessage[0].state);
          this.physicianSubmission.get('phyzcode').setValue(res.responseMessage[0].zipCode);
          this.physicianSubmission.get('phycountry').setValue(res.responseMessage[0].country);
          this.physicianSubmission.get('phyphone').setValue(res.responseMessage[0].phone);
          this.physicianSubmission.get('phyemail').setValue(res.responseMessage[0].email);
          this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
        } else {
          console.log('error');
        }
      });
  }


  // Get Physician Details
  doctordetails(selectdins: any) {
    this._SampleService.getdoctorsdetails(selectdins, this.physicianId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.doctorsdet = res.responseMessage;
          this.phyInsId = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
          this._SampleService.getdoctors(this.phyInsId, this.physicianId)
            .pipe(first())
            .subscribe((res: any) => {
              if (res) {
                this.doctors = res.responseMessage;
               
                this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
              } else {
                console.log('error');
              }
            });
          if (this.currentrole != 'Oncologist') {
            this.physicianSubmission.get('phyname').setValue('');
          }
        } else {
          console.log('error');
        }
      });
  }

  billinsti(selectedinsti:any) {
    var bill = selectedinsti;
  }


  // Select Pathologist
  selectpathologistins(selectdins: any, pathinsti: any, pathfax: any, pathphone: any) {
    this.showtext = false;
    this.pathinstid = selectdins;
    this.patholoinsti = pathinsti;
    let fax = pathfax;
    let phone = pathphone;
    this.pathologistSelection.get('phone').setValue(phone);
    this.pathologistSelection.get('fax').setValue(fax);
    this.pathologistSelection.get('email').setValue(null);
    this.pathologistSelection.get('pathname').setValue(null);
    this.pathologistSelection.get('othername').setValue(null);
    if (pathinsti === 'Other') {
      this.showtextinsti = true;
      this.ReadOnly = false;
    } else {
      this.showtextinsti = false;
      this.ReadOnly = true;
    }
    this._SampleService.getpathologist(selectdins)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologists = res.responseMessage;
          this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].pathologistInstitutionId : 0;
        } else {
          console.log('error');
        }
      });
  }


  selectPathologist() {
    this.loading = true;
    this._SampleService.assignpathologist(this.returnsample, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.messagebox = true;
          this.messagecontent = 'Pathologist Assigned Successfully !';
          this.loading = false;
        }
      });
  }

  // Get pathologist Details
  pathologistdetails(selectppath: any, otherinstiname: any) {
    this.pathnameid = selectppath;
    this.pathologyname = otherinstiname;
    if (otherinstiname === 'Other') {
      this.showtext = true;
    } else {
      this.showtext = false;
    }
    this._SampleService.getpathologistdetails(this.institutionID, selectppath)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologistdet = res.responseMessage[0];
          this.pathologistSelection.get('email').setValue(res.responseMessage[0].email);
        } else {
          console.log('error');
        }
      });
  }



  spchange(checkedvalue: any, event: any) {
    if (event.checked) {
      this.spepanel.push(checkedvalue); 
    
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else {
      const index = this.spepanel.indexOf(checkedvalue);
      this.spepanel.splice(index, 1);
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    }
    if ((this.specimenSubmission.valid == true) && (this.spepanel.length > 0)) {
      this.disablebtn = true;
    }
    else {
      this.disablebtn = false;
    }
  }


  showbilling(selvalue) {
    if (selvalue === 'insurance') {
      this.insti = false;
      this.insur = true;
      this.self_pay = false;
    } else if (selvalue === 'institution_or_study') {
      this.insti = true;
      this.insur = false;
      this.self_pay = false;
    } else if (selvalue === 'self_pay') {
      this.insti = false;
      this.insur = false;
      this.self_pay = true;
    }
  }

  // POST Sample
  showdatefield(selected){
    if(selected === 'in_patient'){
    this.showtextbox = true;
    } else if(selected === 'non_hostpital_patient'){
      this.showtextbox = false;
    } else if(selected === 'out_patient'){
      this.showtextbox = false;
    }
  }

  sampleSubmit(stepper, billingname) {
    if (billingname === 'billing_insure') {
      if (!this.billinginsureSubmission.valid) {
        stepper.selectedIndex = 3;
        return false;
      } else {
        this.billingformselect = this.billinginsureSubmission;
      }
    } else if (billingname === 'billing_insti') {
      if (!this.billinginstiSubmission.valid) {
        stepper.selectedIndex = 3;
        return false;
      } else {
        this.billingformselect = this.billinginstiSubmission;
      }
    } else if (billingname === 'billing_self') {
      this.billingformselect = this.billingSubmission;
    }
    this._SampleService.createSample(
      this.patientSubmission.value,
      this.physicianSubmission.value,
      this.specimenSubmission.value,
      this.billingSubmission.value,
      this.billingformselect.value,
      this.spepanel)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          this.buttonDisabled = true;
          $('#sampleSubmitPreview').modal();
          this.previewvalues = res.responseMessage;
          this.returnsample = res.responseMessage.sampleId;
          this.trans_speciPanel = [];
          for (let i = 0; i < res.responseMessage.specimenPanel.length; i++) {
            const speeci = res.responseMessage.specimenPanel[i];
            if (speeci.isSelected === true) {
              const spdata = {
                'specimenId': speeci.specimenId,
                'panelId': speeci.panelId,
                'panelName': speeci.panelName,
                'isSelected': speeci.isSelected
              };
              this.trans_speciPanel.push(spdata);
            }
          }
          // this.messagebox = true;
          // this.messagecontent = 'Sample Saved Successfully !';
        } else {
          console.log('error');
        }
      });

  }

  closemessagebox() {
    this.messagebox = false;
    this.router.navigate(['/Jacksonlab/samplelist/MySample']);
    this.buttonDisabled = false;
    // $('.modal').modal('hide');
  }

  sampleSubmitlims() {

    this.isError = false;

    //If Pathologist Other
    if (this.pathologyname === 'Other') {

      //Name
      if (this.pathologistSelection.get('othername').value === '' || this.pathologistSelection.get('othername').value === null) {
        this.isError = true;
        this.othervalidshow = true;
      }
      else {
        this.othervalidshow = false;
      }

      //Email
      if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
        this.isError = true;
        this.othervalidemail = true;
      }
      else {
        this.othervalidemail = false;
      }

    }

    //If Institution Other
    if (this.patholoinsti == 'Other') {
      if (this.pathologistSelection.get('otherinstiname').value === '' || this.pathologistSelection.get('otherinstiname').value === null) {
        this.isError = true;
        this.otherinstivalidshow = true;
      }
      else {
        this.otherinstivalidshow = false;
      }
      if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
        this.isError = true;
        this.otherinstifax = true;
      }
      else {
        this.otherinstifax = false;
      }
    }

    if (!this.isError) {
      this.otherinstivalidshow = false;
      this.othervalidshow = false;
      this.othervalidemail = false;
      this.otherinstifax = false;
     
      this.loader = true;
      this.assignpathmsg = false;
      this._SampleService.previewsubmit(this.returnsample, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
        .subscribe((result: any) => {
          if (!result.isError) {
            
            this.loader = false;
            $('#sampleSubmitPreview').modal('hide');
            this.messagebox = true;
            this.messagecontent = 'Sample Submitted Successfully !';
          }
          else {
            this.loader = false;
            this.assignpathmsg = true;
            this.assignpatherror = result.errorMessage;
          }
        });
    }
    
  }

  pathologyassign() {
        this.isError = false;

        //If Pathologist Other
        if (this.pathologyname === 'Other') {

          //Name
          if (this.pathologistSelection.get('othername').value === '' || this.pathologistSelection.get('othername').value === null) {
            this.isError = true;
            this.othervalidshow = true;
          }
          else {
            this.othervalidshow = false;
          }

          //Email
          if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
            this.isError = true;
            this.othervalidemail = true;
          }
          else {
            this.othervalidemail = false;
          }


        }

        //If Institution Other
        if (this.patholoinsti == 'Other') {
          if (this.pathologistSelection.get('otherinstiname').value === '' || this.pathologistSelection.get('otherinstiname').value === null) {
            this.isError = true;
            this.otherinstivalidshow = true;
          }
          else {
            this.otherinstivalidshow = false;
          }
          if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
            this.isError = true;
            this.otherinstifax = true;
          }
          else {
            this.otherinstifax = false;
          }
        }

        if (!this.isError) {
          this.otherinstivalidshow = false;
          this.othervalidshow = false;
          this.othervalidemail = false;
          this.otherinstifax = false;
          this.loader = true;
          this.assignpathmsg = false;
          this._SampleService.assignpath(this.returnsample, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
            .subscribe((result: any) => {
              if (!result.isError) {
               
                this.loader = false;
                $('#sampleSubmitPreview').modal('hide');
                this.messagebox = true;
                this.messagecontent = 'Sample Submitted Successfully !';
              }
              else {
                this.loader = false;
                this.assignpathmsg = true;
                this.assignpatherror = result.errorMessage;
              }
            });
        }
    
  }



 
}

