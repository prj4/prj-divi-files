import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SamplesubmissionComponent } from './samplesubmission.component';

describe('SamplesubmissionComponent', () => {
  let component: SamplesubmissionComponent;
  let fixture: ComponentFixture<SamplesubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SamplesubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SamplesubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
