import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import { ConstantPool } from '@angular/compiler';
import { SampleService } from '../../../_services/sample.service';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
declare var $: any;

export interface Pendingreview {
  createdat: number;
  name: string;
  phone: number;
  institution: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  loading = true;
  loads = false;
  individualTabfooter: string[] = ['viewmore'];
  pendingReviewFooter: string[] = [ 'viewmore']
  cellcontent: FormGroup;
  day_count: number;
  week_count: Number;
  total_count: Number;
  pending_submission: Number;
  revsampleId: number;
  activities: string;
  queue_count: any;
  reviewbutton: any;
  submitbutton: any;
  dataSource;
  currentrole: string;
  getDetails: any;
  getstate: string;
  individualTab: string[];
  pendingReviewColumns: string[];
  showtextbox: boolean;
  reviewfiles: File[] = [];
  getpendingstate: any;


  @ViewChild('reviewfile')
  reviewfile: ElementRef;

  
  disablebtn: boolean;
  fileToUpload: File[] = [];
  spefeatures: any = [];
  pathsampleId: any;
  spefe: any;
  othercomment: any;
  showerror: boolean;
  errormsg: string;
  filetype: string;
  messagebox: boolean;
  messagecontent: string;
  datalength: any;
  datalengthsubmit: any;
  datalengthreview: any;
  datasubmit: any;
  datareview: any;
  getlength: any;
  
 

  constructor(
    private _SampleService: SampleService,
    private adalSvc: MsAdalAngular6Service,
    private _formBuilder: FormBuilder
    ) { }


  ngOnInit() {
    this.getpendingstate = 'PR';
    this.individualTab = [
      'id',
      'name',
      'sex',
      'pname',
      'institution',
      'view'
    ];
    if (this.currentrole === 'Pathologist') {
      this.pendingReviewColumns = [
        'id',
        'name',
        'sex',
        'pname',
        'institution',
        'Review'
      ];
    } else if ((this.currentrole === 'Oncologist')|| (this.currentrole === 'StudyCoOrdinator')) {
      this.pendingReviewColumns = [
        'id',
        'name',
        'sex',
        'pname',
        'institution',
        'action'
      ];
    }
   

    this.cellcontent = this._formBuilder.group({
      cellcontentoptone: ['circled_area', Validators.required],
      cellcontentopttwo: ['30_50_cancer_cells', Validators.required],
      specfeaturesother: [''],
      reviewfile: ['']
    });

    this._SampleService.getdashboard()
      .subscribe((result: any) => {
        this.loading = false;
        this.currentrole = this.adalSvc.userInfo.profile.roles[0];
        this.day_count = result.responseMessage.dayCount;
        this.week_count = result.responseMessage.weekCount;
        this.total_count = result.responseMessage.totalCount;
        this.pending_submission = result.responseMessage.pendingSubmitCount;
        this.activities = result.responseMessage.activity;
        this.queue_count = result.responseMessage.queueCount;

        this.dataSource = new MatTableDataSource(result.responseMessage.pendingSubmit);
        this.datalengthsubmit = this.dataSource;
        if (this.datalengthsubmit.data.length === 0) {
          this.datalengthsubmit = true;
        }
        else {
          this.datalengthsubmit = false;
        }
       
        if (this.currentrole === 'Pathologist') {
          this.dataSource = new MatTableDataSource(result.responseMessage.pendingReview);
          this.reviewbutton = true;
          this.submitbutton = false;
          this.datalengthreview = this.dataSource;
          if (this.datalengthreview.data.length === 0) {
            this.datalengthreview = true;
          }
          else {
            this.datalengthreview = false;
          }
        }
        this._SampleService.getdashboardwizard('ReportsSubmitted')
        .pipe(first())
        .subscribe((res: any) => {
          if (res) {
            this.getDetails = new MatTableDataSource(res.responseMessage);
            if (this.getDetails.data.length === 0) {
              this.getlength = true;
            }
            else {
              this.getlength = false;
            }
            this.getstate = 'ReportsSubmitted';
          } else {
            console.log('error');
          }
        });
      });
  }

  changewizard(changestype) {
    this._SampleService.getdashboardwizard(changestype)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.getDetails = new MatTableDataSource(res.responseMessage);
          if (this.getDetails.data.length === 0) {
            this.getlength = true;
          }
          else {
            this.getlength = false;
          }
          this.getstate = changestype;
        } else {
          console.log('error');
        }
      });
  }

  pathpendstate(state: string) {
    if (state === 'PR') {
      this.loading = true;
      this._SampleService.getdashboard()
        .subscribe((result: any) => {
          this.getpendingstate = 'PR';
          this.pendingReviewColumns = [
            'id',
            'name',
            'sex',
            'pname',
            'institution',
            'Review'
          ];
          this.loading = false;
          this.dataSource = new MatTableDataSource(result.responseMessage.pendingReview);
          this.reviewbutton = true;
          this.submitbutton = false;
          this.datalengthreview = this.dataSource;
          if (this.datalengthreview.data.length === 0) {
            this.datalengthreview = true;
          }
          else {
            this.datalengthreview = false;
          }
        });
    }
    else if (state === 'PS') {
      this.loading = true;
      this._SampleService.getdashboard()
        .subscribe((result: any) => {
          this.getpendingstate = 'PS';
          this.pendingReviewColumns = [
            'id',
            'name',
            'sex',
            'pname',
            'institution',
            'action'
          ];
          this.loading = false;
          this.dataSource = new MatTableDataSource(result.responseMessage.pendingSubmit);
          this.submitbutton = true;
          this.reviewbutton = false;
          this.datalengthsubmit = this.dataSource;
          if (this.datalengthsubmit.data.length === 0) {
            this.datalengthsubmit = true;
          }
          else {
            this.datalengthsubmit = false;
          }
        });
    }
  }


  

  sfchange(checkedvalue: any, event: any) {
    if (checkedvalue === 5) {
      this.showtextbox = true;
    } 
    if (event.checked) {
      this.spefeatures.push(checkedvalue);
      if (this.spefeatures.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else {
      const index = this.spefeatures.indexOf(checkedvalue);
      if (this.spefeatures[index] === 5) {
        this.showtextbox = false;
      } 
      this.spefeatures.splice(index, 1);
      if (this.spefeatures.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    }
  }


  handlereviewFileInput(e) {
    for (var i = 0; i < e.target.files.length; i++) {
      this.fileToUpload.push(e.target.files[i]);
    }
  }

  removefile(e) {
    const index = this.fileToUpload.indexOf(e);
    this.fileToUpload.splice(index, 1);
  }

  pathreview(sampleId) {
    this.fileToUpload = [];
    $('#pathreview').modal('show');
    this.pathsampleId = sampleId;
    this._SampleService.getuser(sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.revsampleId = res.responseMessage.sampleId;
          this.reviewfiles = res.responseMessage.pathologistReview.length === 0 ? null : res.responseMessage.pathologistReview.fileName;
          const cellcontent = res.responseMessage.pathologistReview;
          this.spefe = res.responseMessage.pathologistReview.specimenFeature;
          this.cellcontent.get('cellcontentoptone').setValue(cellcontent.neoplasticCellOne);
          this.cellcontent.get('cellcontentopttwo').setValue(cellcontent.neoplasticCellTwo);
          this.spefeatures = [];
          for (let j = 0; j < this.spefe.length; j++) {
            if (this.spefe[j].isSelected === true) {
              this.spefeatures.push(this.spefe[j].specimenFeatureId);
              if (this.spefe[j].specimenFeatureId === 5) {
                this.othercomment = this.spefe[j].otherComment;
                this.cellcontent.get('specfeaturesother').setValue(this.othercomment);
                this.showtextbox = true;
              }
            }
          }
        }
      });
  }
  submitReview() {
    if (this.reviewfiles === null) {
      if (this.fileToUpload.length === 0) {
        this.showerror = true;
        this.errormsg = 'File Upload is Required';
      } else {
        for (let i = 0; i < this.fileToUpload.length; i++) {
          this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
        }
        if (this.filetype === 'pdf') {
          this.showerror = false;
          this.loads = true;
          this._SampleService.reviewSubmit(
            this.cellcontent.value,
            this.spefeatures,
            this.pathsampleId,
            this.fileToUpload
          )
            .subscribe((data: any) => {
              if (data) {
                this.loads = false;
                $('#pathreview').modal('hide');
                this.messagebox = true;                
                this.messagecontent = 'Review Submitted Successfully !';
                this.ngOnInit();
                this.reviewfile.nativeElement.value = '';

              } else {
                this.loads = false;
              }
            });
        } else {
          this.showerror = true;
          this.errormsg = 'Invalid file type';
        }
      }
    } else {
      this.reviewfile.nativeElement.value = '';
      $("[name=reviewfile]").val(null);
      let x = $("[name=reviewfile]").val('');
     
      if (this.fileToUpload.length === 0) {
        this.showerror = false;
        this.loads = true;
        this._SampleService.reviewSubmit(
          this.cellcontent.value,
          this.spefeatures,
          this.pathsampleId,
          this.fileToUpload
        )
          .subscribe((data: any) => {
            if (data) {
              this.loads = false;
              $('#pathreview').modal('hide');             
              this.messagebox = true;
              this.messagecontent = 'Review Submitted Successfully !';
              this.ngOnInit();
            } else {
              this.loads = false;
            }
          });
      } else {
        if (this.fileToUpload.length != 0) {
          this.reviewfiles = [];
          for (let i = 0; i < this.fileToUpload.length; i++) {
            this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
          }
          if (this.filetype === 'pdf') {
            this.showerror = false;
            this.loads = true;
            this._SampleService.reviewSubmit(
              this.cellcontent.value,
              this.spefeatures,
              this.pathsampleId,
              this.fileToUpload
            )
              .subscribe((data: any) => {
                if (data) {
                  this.loads = false;
                  this.reviewfile.nativeElement.value = '';
                  $('#pathreview').modal('hide');
                  this.messagebox = true;
                  this.messagecontent = 'Review Submitted Successfully !';
                  this.ngOnInit();
                } else {
                  this.loads = false;
                }
              });
          }
        }
      }
    }
  }

  saveReview() {
    if (this.reviewfiles === null) {
      if (this.fileToUpload.length === 0) {
        this.showerror = true;
        this.errormsg = 'File Upload is Required';
      } else {
        for (let i = 0; i < this.fileToUpload.length; i++) {
          this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
        }
        if (this.filetype === 'pdf') {
          this.showerror = false;
          this.loads = true;
          this._SampleService.reviewSave(
            this.cellcontent.value,
            this.spefeatures,
            this.pathsampleId,
            this.fileToUpload
          )
            .subscribe((data: any) => {
              if (data) {
                this.loads = false;
                this.reviewfile.nativeElement.value = '';
                $('#pathreview').modal('hide');
                this.messagebox = true;
                this.messagecontent = 'Review Saved Successfully !';
                this.ngOnInit();
              } else {
                this.loads = false;
              }
            });
        } else {
          this.showerror = true;
          this.errormsg = 'Invalid file type';
        }
      }
    } else {
      this.reviewfile.nativeElement.value = '';
      let x = $("[name=reviewfile]").val('');
      //console.log(x);
      //console.info(this.fileToUpload);

      if (this.fileToUpload.length === 0) {
        this.showerror = false;
        this.loads = true;
        this._SampleService.reviewSave(
          this.cellcontent.value,
          this.spefeatures,
          this.pathsampleId,
          this.fileToUpload
        )
          .subscribe((data: any) => {
            if (data) {
              this.loads = false;
              $('#pathreview').modal('hide');
              this.messagebox = true;
              this.messagecontent = 'Review Saved Successfully !';
              this.ngOnInit();
            } else {
              this.loads = false;
            }
          });
      } else {
        if (this.fileToUpload.length != 0) {
          this.reviewfiles = [];
          for (let i = 0; i < this.fileToUpload.length; i++) {
            this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
          }
          if (this.filetype === 'pdf') {
            this.showerror = false;
            this.loads = true;
            this._SampleService.reviewSave(
              this.cellcontent.value,
              this.spefeatures,
              this.pathsampleId,
              this.fileToUpload
            )
              .subscribe((data: any) => {
                if (data) {
                  this.loads = false;
                  this.reviewfile.nativeElement.value = '';
                  $('#pathreview').modal('hide');
                  this.messagebox = true;
                  this.messagecontent = 'Review Saved Successfully !';
                  this.ngOnInit();
                } else {
                  this.loads = false;
                }
              });
          } 
        }
      }
    }
  }

  deletefile(file) {
    this._SampleService.deletefile(this.revsampleId, file.fileName)
      .subscribe((data: any) => {
        if (data) {
          const index = this.reviewfiles.indexOf(file);
          this.reviewfiles.splice(index, 1);
          //this.messagebox = true;
          //this.messagecontent = 'File Deleted Successfully !';
        }
      });
  }

  closemessagebox() {
    this.messagebox = false;
    $('.modal').modal('hide');

  }
}
