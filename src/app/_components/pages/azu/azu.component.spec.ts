import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AzuComponent } from './azu.component';

describe('AzuComponent', () => {
  let component: AzuComponent;
  let fixture: ComponentFixture<AzuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AzuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AzuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
