import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { SampleService } from '../../../_services/sample.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterModule, Routes } from '@angular/router';
import { first, retry } from 'rxjs/operators';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { ErrorHandler } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
//import { ViewChild } from '@angular/core';

declare var $: any;


export interface SampleSub {
  id: number;
  patientname: string;
  phone: number;
  physicianname: string;
  NPI: string;
  practice: string;
  pathreview: string;
 
}
/**
 * @title Table with filtering
 */
@Component({
  selector: 'app-samplelist',
  templateUrl: './samplelist.component.html',
  styleUrls: ['./samplelist.component.css']
})


export class SamplelistComponent implements OnInit {
  loading = true;
  dataSource;
  deleteokk: boolean;
  deleteokkid: any;
  selectedOption: any;
  specimenfeatures: any;
  spefeatures: any = [];
  disablebtn: boolean;
  messagebox: boolean;
  messagecontent: string;
  showtextbox: boolean;
  fileToUpload: File []= [];
  reviewfileup: any = [];
  deleteconfirm: boolean;
  reviewfetchdet: any;
  displayedColumns: any;
  spefe: any;
  othercomment: any;
  showerror = false;
  isError: boolean;
  errormsg: string;
  pathinstid: any;
  pathnameid: any;
  pathologyname: string;
  hide1: boolean = false;
  hide2: boolean = false;
  hide: boolean = false;
  hide5: boolean = false;
  buttonDisabled: boolean;
  othervalidemail: boolean;
  showtextinsti: boolean;
  assignpathmsg: boolean;

  get user(): any {
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('reviewfile')
  reviewfile: ElementRef;

  sampleId: string;
  revsampleId: number;
  no: any;
  size: any;
  pathologistSelection: FormGroup;
  totallength: number;
  pageSizeOptions: any;
  pathologistsites: [];
  pathologists: [];
  pathsampleId: Number;
  cellcontent: FormGroup;
  pathologistdet: [];
  pageIndex: number;
  pageSize: number;
  filter: string;
  currentrole: string;
  stateofins: boolean;
  patholoinsti: string;
  sfee: [];
  institutionID: any;
  filetype: string;
  reviewfiles: File[] = [];
  listdata: any;
  df: any;
  showtext: boolean;
  othervalid: string;
  othervalidshow: boolean;
  otherinstivalidshow: boolean;
  otherinstifax: boolean;
  assignpatherror: string;
  disableurl: boolean;
  ReadOnly: boolean;
  pathinsituteId: number;

  constructor(
    private _SampleService: SampleService,
    private adalSvc: MsAdalAngular6Service,
    private _formBuilder: FormBuilder,
    private router: Router,
    private _Activatedroute: ActivatedRoute) {
    this.filter = this._Activatedroute.snapshot.url[1].path;
    this.messagebox = false;
    this.showtextbox = false;
    this.deleteconfirm = false;
    this.currentrole = this.adalSvc.userInfo.profile.roles[0];
    this.showtext = false;
    this.othervalidshow = false;
    this.buttonDisabled = true;
    this.showtextinsti = false;
    this.otherinstivalidshow = false;
    this.otherinstifax = false;
    this.isError = false;
    this.assignpathmsg = false;
    this.disableurl = false;
    this.ReadOnly = false;
  }

  ngOnInit() {
    if (this.currentrole === 'Oncologist') {
      this.displayedColumns = [
        'id',
        'specimenId',
        'patientName',
        'institution',
        'pathologistinstitute',
        'pathologistname',
        'rstatus',
        'assign',
        'view',
        'action'
      ];
    } else if (this.currentrole === 'Pathologist') {
      this.displayedColumns = [
        'id',
        'specimenId',
        'patientName',
        'institution',
        'physicianName',
        'rstatus',
        'review',
        'view',
        'action'
      ];

    } else {
      this.displayedColumns = [
        'id',
        'specimenId',
        'patientName',
        'institution',
        'physicianName',
        'pathologistname',
        'rstatus',
        'assign',
        'view',
        'action'
      ];

    }

    this.selectedOption = this.filter;
    this.currentrole = this.adalSvc.userInfo.profile.roles[0];
    var getsessionlist;
    this.df = sessionStorage.setItem('samplelistsessions', this.filter);

    if (this.filter != 'MySample') {
      getsessionlist = sessionStorage.getItem('samplelistsessions');
    } else {
      getsessionlist = sessionStorage.getItem('samplelistsession');
    }
    
    if (getsessionlist === 'nolist') {
      this.selectedOption = 'MySample';
    } else {
      this.selectedOption = getsessionlist;
    }

    if (this.currentrole === 'Pathologist') {
      if (this.filter != 'All') {
        getsessionlist = sessionStorage.getItem('samplelistsessions');
      } else {
        getsessionlist = sessionStorage.getItem('samplelistsession');
      }
        if (getsessionlist === 'nolist') {
            this.selectedOption = 'All';
        } else {
          this.selectedOption = getsessionlist;
          }
    }

   

    this._SampleService.samplebyfilter(this.selectedOption)
      .subscribe((data: any) => {
        if (data) {
          this.loading = false;
          this.dataSource = new MatTableDataSource(data.responseMessage);
          setTimeout(() => this.dataSource.paginator = this.paginator);
          if (this.dataSource.data.length === 0) {
            this.listdata = true;
          }
          else {
            this.listdata = false;
          }
        } else {
          this.loading = true;
        }
      });

    // Get Pathologist Institution
    this._SampleService.getpathologistinstitution()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologistsites = res.responseMessage;
        } else {
          console.log('error');
        }
      });

    this.cellcontent = this._formBuilder.group({
      cellcontentoptone: ['circled_area', Validators.required],
      cellcontentopttwo: ['30_50_cancer_cells', Validators.required],
      specfeaturesother: [''],
      reviewfile: ['']
    });

    this.pathologistSelection = this._formBuilder.group({
      pathinstution: ['', Validators.required],
      otherinstiname:[''],
      pathname: ['', Validators.required],
      othername:[''],
      phone: ['', Validators.pattern],
      email: [''],
      fax: ['', Validators.pattern]
    });

    this._SampleService.getpathreviewcontent()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.specimenfeatures = res.responseMessage;
        } else {
          console.log('error');
        }
      });


  }
  listfilter(dataval) {
    this._SampleService.samplebyfilter(dataval)
      .subscribe((data: any) => {
        if (data) {
          this.loading = false;
          sessionStorage.setItem('samplelistsession', dataval);
          this.dataSource = new MatTableDataSource(data.responseMessage);
          setTimeout(() => this.dataSource.paginator = this.paginator);
          if (this.dataSource.data.length === 0) {
            this.listdata = true;
          }
          else {
            this.listdata = false;
          }
      
        } else {
          this.loading = true;
        }
      });
  }

  deleteok(sampleId) {
    this.deleteconfirm = true;
    this.deleteokkid = sampleId;
  }

  // Select Pathologist
  selectpathologistint(selectdins: any, pathinsti: any, pathfax: any, pathphone: any) {
    this.showtext = false;
    this.pathinstid = selectdins;
    this.patholoinsti = pathinsti;
    let fax = pathfax;
    let phone = pathphone;
    this.pathologistSelection.get('phone').setValue(phone);
    this.pathologistSelection.get('fax').setValue(fax);
    this.pathologistSelection.get('email').setValue(null);
    this.pathologistSelection.get('pathname').setValue(null);
    this.pathologistSelection.get('othername').setValue(null);
    if (pathinsti === 'Other') {
      this.showtextinsti = true;
      this.ReadOnly = false;
    } else {
      this.showtextinsti = false;
      this.ReadOnly = true;
    }

    this._SampleService.getpathologist(selectdins)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologists = res.responseMessage;
          this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].pathologistInstitutionId : 0;
        } else {
          console.log('error');
        }
      });
  }

  // Submit Pathologist
  pathassign(sampleId) {
    $('#pathologistassign').modal('show');
    this.othervalidemail = false;
    this.othervalidshow = false;
    this.showtext = false;
    this.otherinstivalidshow = false;
    this.otherinstifax = false;
    this.showtextinsti = false;
    this._SampleService.getuser(sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          if (res.responseMessage.assignedPathologist !== null) {
            this.reviewfetchdet = res.responseMessage.assignedPathologist;
            this.pathologistSelection.get('email').setValue(this.reviewfetchdet.email);
            this.pathologistSelection.get('phone').setValue(this.reviewfetchdet.phone);
            this.pathologistSelection.get('fax').setValue(this.reviewfetchdet.fax);
            this.pathinsituteId = res.responseMessage.assignedPathologist.pathologistInstitutionId;
            this.pathologistSelection.get('pathinstution').setValue(res.responseMessage.assignedPathologist.pathologistInstitutionId);
            if (res.responseMessage.assignedPathologist.pathologistId != '0') {
              this.pathologistSelection.get('pathname').setValue(res.responseMessage.assignedPathologist.pathologistId);
            } else {
              this.pathologistSelection.get('pathname').setValue('');
            }
            this._SampleService.getpathologist(res.responseMessage.assignedPathologist.pathologistInstitutionId)
              .pipe(first())
              .subscribe((ress: any) => {
                if (ress) {
                  this.pathologists = ress.responseMessage;

                } else {
                  console.log('error');
                }
              });

          } else {
            this.pathologistSelection = this._formBuilder.group({
              pathinstution: [''],
              otherinstiname:[''],
              pathname: [''],
              othername:[''],
              phone: ['', Validators.pattern],
              email: ['', [Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
              fax: ['', Validators.pattern]
            });
          }
        }
      });
    this.pathsampleId = sampleId;
  }

  // Submit Pathologist Review
  pathreview(sampleId) {
    this.fileToUpload = [];
    $('#pathreview').modal('show');
    this.pathsampleId = sampleId;
    this._SampleService.getuser(sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.revsampleId = res.responseMessage.sampleId;
          this.reviewfiles = res.responseMessage.pathologistReview.length === 0 ? null : res.responseMessage.pathologistReview.fileName;
          const cellcontent = res.responseMessage.pathologistReview;
          this.spefe = res.responseMessage.pathologistReview.specimenFeature;
          this.cellcontent.get('cellcontentoptone').setValue(cellcontent.neoplasticCellOne);
          this.cellcontent.get('cellcontentopttwo').setValue(cellcontent.neoplasticCellTwo);
          this.spefeatures = [];
          for (let j = 0; j < this.spefe.length; j++) {
            if (this.spefe[j].isSelected === true) {
              this.spefeatures.push(this.spefe[j].specimenFeatureId);
              if (this.spefe[j].specimenFeatureId === 5) {
                this.othercomment = this.spefe[j].otherComment;
                this.cellcontent.get('specfeaturesother').setValue(this.othercomment);
                this.showtextbox = true;
              }
            }

          }
        }
      });
  }

  selectPathologist() {
    this.isError = false;
    if (this.pathologyname === 'Other') {

      if (this.pathologistSelection.get('othername').value === '' || this.pathologistSelection.get('othername').value === null) {
        this.isError = true;
        this.othervalidshow = true;
      }
      else {
        this.othervalidshow = false;
      }
      if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
        this.isError = true;
        this.othervalidemail = true;
      }
      else {
        this.othervalidemail = false;
      }
    }

    if (this.patholoinsti === 'Other') {
      if (this.pathologistSelection.get('otherinstiname').value === '' || this.pathologistSelection.get('otherinstiname').value === null) {
        this.isError = true;
        this.otherinstivalidshow = true;
      }
      else {
        this.otherinstivalidshow = false;
      }

      if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
        this.isError = true;
        this.otherinstifax = true;
      }
      else {
        this.otherinstifax = false;
      }
    }

    if (!this.isError) {
      this.otherinstivalidshow = false;
      this.othervalidshow = false;
      this.othervalidemail = false;
      this.otherinstifax = false;
      this.loading = true;
      this.assignpathmsg = false;
      this._SampleService.assignpathologist(this.pathsampleId, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
        .pipe(first())
        .subscribe((res: any) => {
          if (!res.isError) {
           
            $('#pathologistassign').modal('hide');
            this.messagebox = true;
            this.loading = false;
            this.messagecontent = 'Sample Submitted Successfully !';
            this.ngOnInit();

          }
          else {
            this.loading = false;
            this.assignpathmsg = true;
            this.assignpatherror = res.errorMessage;
          }
        }); 
    }
  }


  // Get pathologist Details
  pathologistdetails(selectppath: any, otherinstiname: any) {
    this.pathnameid = selectppath;
    this.pathologyname = otherinstiname;
    if (otherinstiname === 'Other') {
      this.showtext = true;
    } else {
      this.showtext = false;
    }
    if (this.institutionID) {

      this._SampleService.getpathologistdetails(this.institutionID, selectppath)
        .pipe(first())
        .subscribe((res: any) => {
          if (res) {
            this.pathologistdet = res.responseMessage[0];
            this.pathologistSelection.get('email').setValue(res.responseMessage[0].email);
          } else {
            console.log('error');
          }
        });

    } else {
      this._SampleService.getpathologistdetails(this.pathinsituteId, selectppath)
        .pipe(first())
        .subscribe((res: any) => {
          if (res) {
            this.pathologistdet = res.responseMessage[0];
            this.pathologistSelection.get('email').setValue(res.responseMessage[0].email);
          } else {
            console.log('error');
          }
        });

    }
  }

  sfchange(checkedvalue: any, event: any) {
    if (checkedvalue === 5) {
      this.showtextbox = true;
    } 
    if (event.checked) {
      this.spefeatures.push(checkedvalue);
      if (this.spefeatures.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else {
      const index = this.spefeatures.indexOf(checkedvalue);
      if (this.spefeatures[index] === 5) {
        this.showtextbox = false;
      } 
      this.spefeatures.splice(index, 1);
      if (this.spefeatures.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    }
  }

  handlereviewFileInput(e) {
    for (var i = 0; i < e.target.files.length; i++) {
      this.fileToUpload.push(e.target.files[i]);
    }
  }

  removefile(e) {
    const index = this.fileToUpload.indexOf(e);
    this.fileToUpload.splice(index, 1);
  }

  submitReview() {
    if (this.reviewfiles.length === 0) {
      if (this.fileToUpload.length === 0) {
        this.showerror = true;
        this.errormsg = 'File Upload is Required';
      } else {
        for (let i = 0; i < this.fileToUpload.length; i++) {
          this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
        }
        if (this.filetype === 'pdf') {
          this.showerror = false;
          this.loading = true;
          this._SampleService.reviewSubmit(
            this.cellcontent.value,
            this.spefeatures,
            this.pathsampleId,
            this.fileToUpload
          )
            .subscribe((data: any) => {
              if (data) {
                this.loading = false;
                $('#pathreview').modal('hide');
                this.messagebox = true;                
                this.messagecontent = 'Review Submitted Successfully !';
                this.ngOnInit();
                this.reviewfile.nativeElement.value = '';

              } else {
                this.loading = false;
              }
            });
        } else {
          this.showerror = true;
          this.errormsg = 'Invalid file type';
        }
      }
    } else {
      this.reviewfile.nativeElement.value = '';
      $("[name=reviewfile]").val(null);
      let x = $("[name=reviewfile]").val('');
     
      if (this.fileToUpload.length === 0) {
        this.showerror = false;
        this.loading = true;
        this._SampleService.reviewSubmit(
          this.cellcontent.value,
          this.spefeatures,
          this.pathsampleId,
          this.fileToUpload
        )
          .subscribe((data: any) => {
            if (data) {
              this.loading = false;
              $('#pathreview').modal('hide');             
              this.messagebox = true;
              this.messagecontent = 'Review Submitted Successfully !';
              this.ngOnInit();
            } else {
              this.loading = false;
            }
          });
      } else {
        if (this.fileToUpload.length != 0) {
        //  this.reviewfiles = [];
          for (let i = 0; i < this.fileToUpload.length; i++) {
            this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
          }
          if (this.filetype === 'pdf') {
            this.showerror = false;
            this.loading = true;
            this._SampleService.reviewSubmit(
              this.cellcontent.value,
              this.spefeatures,
              this.pathsampleId,
              this.fileToUpload
            )
              .subscribe((data: any) => {
                if (data) {
                  this.loading = false;
                  this.reviewfile.nativeElement.value = '';
                  $('#pathreview').modal('hide');
                  this.messagebox = true;
                  this.messagecontent = 'Review Submitted Successfully !';
                  this.ngOnInit();
                } else {
                  this.loading = false;
                }
              });
          }
        }
      }
    }
  }

  saveReview() {
    if (this.reviewfiles.length === 0) {
      if (this.fileToUpload.length === 0) {
        this.showerror = true;
        this.errormsg = 'File Upload is Required';
      } else {
        for (let i = 0; i < this.fileToUpload.length; i++) {
          this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
        }
        if (this.filetype === 'pdf') {
          this.showerror = false;
          this.loading = true;
          this._SampleService.reviewSave(
            this.cellcontent.value,
            this.spefeatures,
            this.pathsampleId,
            this.fileToUpload
          )
            .subscribe((data: any) => {
              if (data) {
                this.loading = false;
                this.reviewfile.nativeElement.value = '';
                $('#pathreview').modal('hide');
                this.messagebox = true;
                this.messagecontent = 'Review Saved Successfully !';
                this.ngOnInit();
              } else {
                this.loading = false;
              }
            });
        } else {
          this.showerror = true;
          this.errormsg = 'Invalid file type';
        }
      }
    } else {
      this.reviewfile.nativeElement.value = '';
      let x = $("[name=reviewfile]").val('');
      
      if (this.fileToUpload.length === 0) {
        this.showerror = false;
        this.loading = true;
        this._SampleService.reviewSave(
          this.cellcontent.value,
          this.spefeatures,
          this.pathsampleId,
          this.fileToUpload
        )
          .subscribe((data: any) => {
            if (data) {
              this.loading = false;
              $('#pathreview').modal('hide');
              this.messagebox = true;
              this.messagecontent = 'Review Saved Successfully !';
              this.ngOnInit();
            } else {
              this.loading = false;
            }
          });
      } else {
        if (this.fileToUpload.length != 0) {
        //  this.reviewfiles = [];
          for (let i = 0; i < this.fileToUpload.length; i++) {
            this.filetype = this.fileToUpload[i].name.substr((this.fileToUpload[i].name.lastIndexOf('.') + 1));
          }
          if (this.filetype === 'pdf') {
            this.showerror = false;
            this.loading = true;
            this._SampleService.reviewSave(
              this.cellcontent.value,
              this.spefeatures,
              this.pathsampleId,
              this.fileToUpload
            )
              .subscribe((data: any) => {
                if (data) {
                  this.loading = false;
                  this.reviewfile.nativeElement.value = '';
                  $('#pathreview').modal('hide');
                  this.messagebox = true;
                  this.messagecontent = 'Review Saved Successfully !';
                  this.ngOnInit();
                } else {
                  this.loading = false;
                }
              });
          } 
        }
      }
    }
  }

  deletefile(file) {
    this._SampleService.deletefile(this.revsampleId, file.fileName)
      .subscribe((data: any) => {
        if (data) {
          const index = this.reviewfiles.indexOf(file);
          this.reviewfiles.splice(index, 1);
          //this.messagebox = true;
          //this.messagecontent = 'File Deleted Successfully !';
        }
      });
  }

  closemessagebox() {
    this.messagebox = false;
    this.reviewfile.nativeElement.innerText = '';
    this.deleteconfirm = false;
    $('.modal').modal('hide');
 
   
  }

  deleteSample() {

    this._SampleService.deletesample(this.deleteokkid)
      .subscribe((data: any) => {
        if (data) {
          this.ngOnInit();
          this.deleteconfirm = false;
          this.messagebox = true;
          this.messagecontent = 'Sample Deleted Successfully !';
        }
      });

  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pathologyassign() { 
    this.isError = false;
  
    if (this.pathologyname === 'Other') {

      if (this.pathologistSelection.get('othername').value === '' || this.pathologistSelection.get('othername').value === null) {
        this.isError = true;
        this.othervalidshow = true;
      }
      else {
        this.othervalidshow = false;
      }
      if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
        this.isError = true;
        this.othervalidemail = true;
      }
      else {
        this.othervalidemail = false;
      }
    }
        if (this.patholoinsti == 'Other') {
          if (this.pathologistSelection.get('otherinstiname').value === '' || this.pathologistSelection.get('otherinstiname').value === null) {
            this.isError = true;
            this.otherinstivalidshow = true;
          }
          else {
            this.otherinstivalidshow = false;
          }
          if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
            this.isError = true;
            this.otherinstifax = true;
          }
          else {
            this.otherinstifax = false;
          }
    }
    if (!this.isError) {
      this.otherinstivalidshow = false;
      this.othervalidemail = false;
      this.othervalidshow = false;
      this.otherinstifax = false;
      this.loading = true;
      this.assignpathmsg = false;
      this._SampleService.assignpath(this.pathsampleId, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
        .subscribe((result: any) => {
          if (!result.isError) {
           
            $('#pathologistassign').modal('hide');
            this.loading = false;
            this.ngOnInit();
            this.messagebox = true;
            this.messagecontent = 'Sample Submitted Successfully !';
          }
          else {
            this.loading = false;
            this.assignpathmsg = true;
            this.assignpatherror = result.errorMessage;
          }
        });
      }
  }

  signedsample(sampleid) {
    this.loading = true;
    this._SampleService.signedsample(sampleid)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          const newBlob = new Blob([res], { type: 'application/pdf' });
          const data = window.URL.createObjectURL(res);
          // window.open(data);
          const file_path = data;
          const down = document.createElement('A') as HTMLAnchorElement;
          down.href = file_path;
          down.download = "Sample-".concat(sampleid);
          document.body.appendChild(down);
          down.click();
          document.body.removeChild(down);
        }
      });
  }

  reportDelivered(link) {
    if (link!= null) {
      window.open(link, "_blank");
    }   
  }

}



