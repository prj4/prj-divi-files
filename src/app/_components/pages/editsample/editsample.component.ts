import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, NgForm, FormControl, AbstractControl, EmailValidator } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { first } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';

import { SampleService } from '../../../_services/sample.service';

declare var $: any;

@Component({
  selector: 'app-editsample',
  templateUrl: './editsample.component.html',
  styleUrls: ['./editsample.component.css']
})
export class EditsampleComponent implements OnInit {


  value: string;
  viewValue: string;
  loading = false; isLinear = false;
  patientSubmission: FormGroup;
  physicianSubmission: FormGroup;
  specimenSubmission: FormGroup;
  billinginsureSubmission: FormGroup;
  billinginstiSubmission: FormGroup;
  billingSubmission: FormGroup;
  billingformselect: FormGroup;
  pathologistSelection: FormGroup;
  specimenlist: [];
  institutions: [];
  doctors: [];
  doctorsdet: [];
  spepanel: any[] = [];
  relpat: any;
  radiolist;
  realtpatient;
  paymode;
  gen: '';
  gender: string;
  inss: number;
  pstate: any;
  submissiondetails: any;
  sampleId: number;
  transactions: any;
  specimentransactionspanel: any;
  specimentransactionspanel1: any;
  patienttransactions: any;
  specimentransactions: any;
  billingtransactions: any;
  physiciantransactions: any;
  trans_speciPanel = [];
  previewvalues: any;
  id: any;
  name: string;
  conmessage: string;
  panellistlength: number;
  spformcheckbox: boolean;
  disablebtn: boolean;
  showtextbox: boolean;
  insti: boolean;
  insur: boolean;
  self_pay: boolean;
  currentrole: string;
  billpaymode: string;
  messagebox: boolean;
  messagecontent: string;
  showtext: boolean;
  dobmaxDate = new Date();
  datecollmaxDate = new Date();
  institutionID: number;
  returnsample: number;
  physicianId: number;
  physicianInstiId: number;
  phyInsId: number;
  pathinstid: any;
  pathnameid: any;
  pathologists: [];
  pathologistdet: [];
  pathologistsites: [];
  loader: boolean;
  reviewfetchdata: string;
  buttonDisabled: boolean;
  patholoinsti: string;
  pathologyname: string;
  othervalidshow: boolean;
  othervalidemail: boolean;
  hide3: any;
  hide1: any;
  hide2: any;
  hide: any;
  hide5: any;
  noneselfpay: any;
  billinstitute: [];
  dispatgender: any;
  emailbtn: boolean;
  showtextinsti: boolean;
  otherinstivalidshow: boolean;
  isError: boolean;
  assignpatherror: string;
  assignpathmsg :boolean;
  dateformatev: any;
  otherinstifax: boolean;
  ReadOnly: boolean;
  pathinsituteId: number;
  constructor(
    private _formBuilder: FormBuilder,
    private _SampleService: SampleService,
    private router: Router,
    private adalSvc: MsAdalAngular6Service,
    private _Activatedroute: ActivatedRoute
  ) {
    this.sampleId = this._Activatedroute.snapshot.params.sampleId;
    this.isError = false;
    this.insti = false;
    this.insur = true;
    this.self_pay = false;
    this.messagebox = false;
    this.showtextbox = false;
    this.buttonDisabled = false;
    this.showtext = false;
    this.othervalidshow = false;
    this.othervalidemail = false;
    this.emailbtn = false;
    this.showtextinsti = false;
    this.otherinstivalidshow = false;
    this.assignpathmsg = false;
    this.otherinstifax = false;
    this.ReadOnly = false;

  }

  @ViewChild('stepper') stepper: MatStepper;

  ngOnInit() {

    sessionStorage.setItem('samplelistsessions', 'nolist');
    sessionStorage.setItem('samplelistsession', 'nolist');

    this.currentrole = this.adalSvc.userInfo.profile.roles[0];
    //if (this.currentrole === 'StudyCoOrdinator') {
      //this.router.navigate(['/Jacksonlab/dashboard']);
    //}

    if (this.currentrole === 'Pathologist') {
      this.stepper.selectedIndex = 2;
    }

    this._SampleService.getinstitution()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.institutions = res.responseMessage;
          this.physicianId = res.responseMessage.length !== 0 ? res.responseMessage[0].physicianId : 0;
          this.physicianInstiId = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
        } else {
          console.log('error');
        }
      });


    // Get Billing Institution
    this._SampleService.getbillinstitute()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.billinstitute = res.responseMessage;
        }
        else {
          console.log('error');
        }
      })

      this._SampleService.getpathologistinstitution()
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologistsites = res.responseMessage;
        } else {
          console.log('error');
        }
      });



    this._SampleService.getuser(this.sampleId)
      .pipe(first())
      .subscribe((data: any) => {
        if (data) {


          const actualdate = data.responseMessage.patient.dob;
          let changeddate = actualdate + 'T00:00:00';
          

          this.billpaymode = data.responseMessage.bill.paymentModeId;
          this.specimentransactionspanel = data.responseMessage.specimenPanel;
          const splen = this.specimentransactionspanel.length;
          this.panellistlength = splen - 1;
          this.patienttransactions = data.responseMessage.patient;
          this.patientSubmission.get('patname').setValue(data.responseMessage.patient.patientName);
          this.patientSubmission.get('patdob').setValue(changeddate);
          this.patientSubmission.get('patgender').setValue(data.responseMessage.patient.sex);
          this.patientSubmission.get('pataddress').setValue(data.responseMessage.patient.address);
          this.patientSubmission.get('patcity').setValue(data.responseMessage.patient.city);
          this.patientSubmission.get('patstate').setValue(data.responseMessage.patient.state);
          this.patientSubmission.get('patzcode').setValue(data.responseMessage.patient.zipcode);
          this.patientSubmission.get('patcountry').setValue(data.responseMessage.patient.country);
          this.patientSubmission.get('patphone').setValue(data.responseMessage.patient.phoneNumber);
          this.patientSubmission.get('patmrecord').setValue(data.responseMessage.patient.medicalRecord);


          this.physiciantransactions = data.responseMessage.physician;

          this._SampleService.getdoctorsdetails(data.responseMessage.physician.institutionId, 0)
            .pipe(first())
            .subscribe((res: any) => {
              if (res) {
                this.doctors = res.responseMessage;
              }
            });

          this.physicianSubmission.get('phyinstution').setValue(data.responseMessage.physician.institutionId);
          this.physicianSubmission.get('phyname').setValue(data.responseMessage.physician.physicianId);
         

          this._SampleService.getdoctors(data.responseMessage.physician.institutionId, data.responseMessage.physician.physicianId)
            .pipe(first())
            .subscribe((res: any) => {
              if (res) {
                this.physicianSubmission.get('phyNPI').setValue(res.responseMessage[0].npi);
                this.physicianSubmission.get('phyaddress').setValue(res.responseMessage[0].address);
                this.physicianSubmission.get('phycity').setValue(res.responseMessage[0].city);
                this.physicianSubmission.get('phystate').setValue(res.responseMessage[0].state);
                this.physicianSubmission.get('phyzcode').setValue(res.responseMessage[0].zipCode);
                this.physicianSubmission.get('phycountry').setValue(res.responseMessage[0].country);
                this.physicianSubmission.get('phyphone').setValue(res.responseMessage[0].phone);
                this.physicianSubmission.get('phyemail').setValue(res.responseMessage[0].email);
              } else {
                console.log('error');
              }
            });
          
         

          this.specimentransactions = data.responseMessage.specimen;

          this.specimenSubmission.get('specimenID').setValue(data.responseMessage.specimen.specimenId);
          this.specimenSubmission.get('datetimecollected').setValue(data.responseMessage.specimen.collectedDate);
          this.specimenSubmission.get('diagnosis').setValue(data.responseMessage.specimen.diagnosis);
          this.specimenSubmission.get('specimensite').setValue(data.responseMessage.specimen.specimenSite);
          this.specimenSubmission.get('pspecimensite').setValue(data.responseMessage.specimen.primarySpecimenSite);
          this.specimenSubmission.get('daterfromstorage').setValue(data.responseMessage.specimen.storageRemovedDate);

          for (let j = 0; j < this.specimentransactionspanel.length; j++) {
            if (this.specimentransactionspanel[j].isSelected === true) {
              this.spepanel.push(this.specimentransactionspanel[j].panelId);
            }

          }

          if (this.specimenSubmission.valid && this.spepanel.length > 0) {
            this.spformcheckbox = false;
            this.disablebtn = true;

          } else {
            this.spformcheckbox = true;
            this.disablebtn = false;

          }


          this.billingSubmission.get('paymentmode').setValue(data.responseMessage.bill.paymentModeId);
          this.billingtransactions = data.responseMessage.bill;
          this.billinginsureSubmission.get('billingcname').setValue(data.responseMessage.bill.companyName);
          this.billinginsureSubmission.get('billingmemberid').setValue(data.responseMessage.bill.memberId);
          this.billinginsureSubmission.get('billinggroup').setValue(data.responseMessage.bill.group);
          this.billinginsureSubmission.get('billinginsuredname').setValue(data.responseMessage.bill.insuredName);
          this.billinginsureSubmission.get('billingcity').setValue(data.responseMessage.bill.city);
          this.billinginsureSubmission.get('billingstate').setValue(data.responseMessage.bill.state);
          this.billinginsureSubmission.get('billingzcode').setValue(data.responseMessage.bill.zipcode);
          this.billinginsureSubmission.get('billingdob').setValue(data.responseMessage.bill.dob);
          this.billinginstiSubmission.get('billinginsstudyname').setValue(data.responseMessage.bill.studyId);
          this.billinginstiSubmission.get('billingaddressstudy').setValue(data.responseMessage.bill.address);
          this.billinginsureSubmission.get('patstatus').setValue(data.responseMessage.bill.patientStatusId);
          this.billinginsureSubmission.get('relatopatient').setValue(data.responseMessage.bill.relationToPatientId);
          this.billinginsureSubmission.get('billingphone').setValue(data.responseMessage.bill.phone);
          this.billinginsureSubmission.get('dischargedate').setValue(data.responseMessage.bill.dischargeDate);
          if (data.responseMessage.bill.paymentModeId === 'insurance') {
            this.insti = false;
            this.insur = true;
            this.self_pay = false;
          } else if (data.responseMessage.bill.paymentModeId === 'institution_or_study') {
            this.insti = true;
            this.insur = false;
            this.self_pay = false;
          } else if (data.responseMessage.bill.paymentModeId === 'self_pay') {
            this.insti = false;
            this.insur = false;
            this.self_pay = true;
          }
          if(data.responseMessage.bill.patientStatusId === 'in_patient'){
            this.showtextbox = true;
          }

        } else {
          console.log('error');
        }
      });


    this.patientSubmission = this._formBuilder.group({
      patname: ['', Validators.required],
      patdob: ['', Validators.required],
      patgender: ['', Validators.required],
      pataddress: [''],
      patcity: [''],
      patstate: [''],
      patzcode: [''],
      patcountry: [''],
      patphone: ['', Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/)],
      patmrecord: ['', Validators.required]
    });
    this.physicianSubmission = this._formBuilder.group({
      phyinstution: ['', Validators.required],
      phyname: ['', Validators.required],
      phyNPI: [''],
      phyaddress: [''],
      phycity: [''],
      phystate: [''],
      phyzcode: [''],
      phycountry: [''],
      phyphone: [''],
      phyemail: [''],
      phyfax: ['']
    });
    this.specimenSubmission = this._formBuilder.group({
      specimenID: [''],
      specimenpanel: [''],
      datetimecollected: [''],
      diagnosis: [''],
      specimensite: [''],
      pspecimensite: [''],
      daterfromstorage: ['']
    });
    this.billinginsureSubmission = this._formBuilder.group({
      billingcname: ['', Validators.required],
      billingmemberid: ['', Validators.required],
      billinggroup: ['', Validators.required],
      billinginsuredname: ['', Validators.required],
      billingcity: ['', Validators.required],
      billingstate: ['', Validators.required],
      billingzcode: ['', Validators.required],
      billingdob: ['', Validators.required],
      billingphone: ['', Validators.required],
      relatopatient: ['self', Validators.required],
      patstatus: ['out_patient', Validators.required],
      dischargedate:['']

    });

    this.billinginstiSubmission = this._formBuilder.group({
      billinginsstudyname: ['', Validators.required],
      billingaddressstudy: ['', Validators.required]
    });
    this.billingSubmission = this._formBuilder.group({
      paymentmode: ['insurance']
    });

    this._SampleService.getpathologistinstitution()
    .pipe(first())
    .subscribe((res: any) => {
      if (res) {
        this.pathologistsites = res.responseMessage;
      } else {
        console.log('error');
      }
    });
    this.pathologistSelection = this._formBuilder.group({
      pathinstution: ['', Validators.required],
      otherinstiname:[''],
      pathname: ['', Validators.required],
      othername: [''],
      phone: ['', Validators.pattern],
      email: ['', [Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      fax: ['', Validators.pattern]
    });

  }



  datec() {
    this.disablebtn = false;
    if (this.specimenSubmission.valid == true) {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else if (this.specimenSubmission.value.datetimecollected === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
    else if (this.specimenSubmission.value.datetimecollected >= new Date()) {
    }
    else {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      }
      else {
        this.disablebtn = false;
      }
      this.disablebtn = false;
    }
  }

  dated() {
    if (this.billinginsureSubmission.value.dischargedate === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
  }


  dates() {
    this.disablebtn = false;
    if (this.specimenSubmission.valid == true) {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else if (this.specimenSubmission.value.daterfromstorage === null) {
      this.dateformatev = "Date Format Should be 'MM/DD/YYYY' ";
    }
    else if (this.specimenSubmission.value.daterfromstorage >= new Date()) {
    }
    else {
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      }
      else {
        this.disablebtn = false;
      }
      this.disablebtn = false;
    }
  }

  specimenvalidation(stepper) {
    if (this.specimenSubmission.valid && this.spepanel.length > 0) {
      this.spformcheckbox = false;
      this.disablebtn = true;
      stepper.next();
    } else {
      this.spformcheckbox = true;
      stepper.selectedIndex = 2;
      this.disablebtn = false;
    }
  }


  selectdoctor(selectdins: any) {
    this._SampleService.getdoctors(this.phyInsId,selectdins)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
          this.physicianSubmission.get('phyNPI').setValue(res.responseMessage[0].npi);
          this.physicianSubmission.get('phyaddress').setValue(res.responseMessage[0].address);
          this.physicianSubmission.get('phycity').setValue(res.responseMessage[0].city);
          this.physicianSubmission.get('phystate').setValue(res.responseMessage[0].state);
          this.physicianSubmission.get('phyzcode').setValue(res.responseMessage[0].zipCode);
          this.physicianSubmission.get('phycountry').setValue(res.responseMessage[0].country);
          this.physicianSubmission.get('phyphone').setValue(res.responseMessage[0].phone);
          this.physicianSubmission.get('phyemail').setValue(res.responseMessage[0].email);
        } else {
          console.log('error');
        }
      });
  }

  doctordetails(selectddoc: any) {
    this._SampleService.getdoctorsdetails(selectddoc, this.physicianId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.doctors = res.responseMessage;
          this.phyInsId = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
          this._SampleService.getdoctors(this.phyInsId, this.physicianId)
            .pipe(first())
            .subscribe((res: any) => {
              if (res) {
                this.doctors = res.responseMessage;
                if (this.currentrole === 'Oncologist') {
                  this.physicianSubmission.get('phyname').setValue(res.responseMessage[0].physicianId);
                  this.physicianSubmission.get('phyNPI').setValue(res.responseMessage[0].npi);
                  this.physicianSubmission.get('phyaddress').setValue(res.responseMessage[0].address);
                  this.physicianSubmission.get('phycity').setValue(res.responseMessage[0].city);
                  this.physicianSubmission.get('phystate').setValue(res.responseMessage[0].state);
                  this.physicianSubmission.get('phyzcode').setValue(res.responseMessage[0].zipCode);
                  this.physicianSubmission.get('phycountry').setValue(res.responseMessage[0].country);
                  this.physicianSubmission.get('phyphone').setValue(res.responseMessage[0].phone);
                  this.physicianSubmission.get('phyemail').setValue(res.responseMessage[0].email);
                }
                this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].institutionId : 0;
              } else {
                console.log('error');
              }
            });
          this.physicianSubmission.get('phyname').setValue('');
          this.physicianSubmission.get('phyNPI').setValue('');
          this.physicianSubmission.get('phyaddress').setValue('');
          this.physicianSubmission.get('phycity').setValue('');
          this.physicianSubmission.get('phystate').setValue('');
          this.physicianSubmission.get('phyzcode').setValue('');
          this.physicianSubmission.get('phycountry').setValue('');
          this.physicianSubmission.get('phyphone').setValue('');
          this.physicianSubmission.get('phyemail').setValue('');

        } else {
          console.log('error');
        }
      });
  }

  spchange(checkedvalue: any, event: any) {
    if (event.checked) {
      this.spepanel.push(checkedvalue);
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    } else {
      const index = this.spepanel.indexOf(checkedvalue);
      this.spepanel.splice(index, 1);
      if (this.spepanel.length > 0) {
        this.disablebtn = true;
      } else {
        this.disablebtn = false;
      }
    }
  }



  showbilling(selvalue) {
    if (selvalue === 'insurance') {
      this.insti = false;
      this.insur = true;
      this.self_pay = false;
    } else if (selvalue === 'institution_or_study') {
      this.insti = true;
      this.insur = false;
      this.self_pay = false;
    } else if (selvalue === 'self_pay') {
      this.insti = false;
      this.insur = false;
      this.self_pay = true;
    }
  }

  // POST Sample
  sampleSubmit(stepper, billingname) {
    if ((billingname == 'billing_insure') || (billingname =='insurance')) {
      if (!this.billinginsureSubmission.valid) {
        stepper.selectedIndex = 3;
        return false;
      } else {
        this.billingformselect = this.billinginsureSubmission;
      }
    } else if ((billingname == 'billing_insti') || (billingname == 'institution_or_study')) {
      if (!this.billinginstiSubmission.valid) {
        stepper.selectedIndex = 3;
        return false;
      } else {
        this.billingformselect = this.billinginstiSubmission;
      }
    } else if ((billingname === 'billing_self') || (billingname == 'self_pay')) {
      this.billingformselect = this.billingSubmission;
    }
    this._SampleService.updateSample(this.patientSubmission.value,
      this.physicianSubmission.value,
      this.specimenSubmission.value,
      this.billingSubmission.value,
      this.billingformselect.value,
      this.spepanel,
      this.sampleId)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.loading = false;
          this.buttonDisabled = true;
          $('#sampleSubmitPreview').modal();
          this.previewvalues = res.responseMessage;
          if (res.responseMessage.assignedPathologist !== null) {
            this.pathologistdet = res.responseMessage.assignedPathologist;
            this.pathologistSelection.get('email').setValue(res.responseMessage.assignedPathologist.email);
            this.pathologistSelection.get('phone').setValue(res.responseMessage.assignedPathologist.phone);
            this.pathologistSelection.get('fax').setValue(res.responseMessage.assignedPathologist.fax);
            this.pathinsituteId = res.responseMessage.assignedPathologist.pathologistInstitutionId;
            this.pathologistSelection.get('pathinstution').setValue(res.responseMessage.assignedPathologist.pathologistInstitutionId);
            if (res.responseMessage.assignedPathologist.pathologistId != '0') {
              this.pathologistSelection.get('pathname').setValue(res.responseMessage.assignedPathologist.pathologistId);
            } else {
              this.pathologistSelection.get('pathname').setValue('');
            }
            this._SampleService.getpathologist(res.responseMessage.assignedPathologist.pathologistInstitutionId)
              .pipe(first())
              .subscribe((ress: any) => {
                if (ress) {
                  this.pathologists = ress.responseMessage;

                } else {
                  console.log('error');
                }
              });
          } else {
            this.pathologistSelection = this._formBuilder.group({
              pathinstution: [''],
              otherinstiname:[''],
              pathname: [''],
              othername:[''],
              phone: ['', Validators.pattern],
              email: ['', [Validators.email, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
              fax: ['', Validators.pattern]
            });
          }
          this.returnsample = res.responseMessage.sampleId;
          this.trans_speciPanel = [];
          for (let i = 0; i < res.responseMessage.specimenPanel.length; i++) {
            const speeci = res.responseMessage.specimenPanel[i];
            if (speeci.isSelected === true) {
              const spdata = {
                'specimenId': speeci.specimenId,
                'panelId': speeci.panelId,
                'panelName': speeci.panelName,
                'isSelected': speeci.isSelected
              };
              this.trans_speciPanel.push(spdata);
            }
          }
          // this.messagebox = true;
          // this.messagecontent = 'Sample Updated Successfully !';
        } else {
          console.log('error');
        }
      });

  }

  showdatefield(selected){
    if(selected === 'in_patient'){
    this.showtextbox = true;
    } else if(selected === 'non_hostpital_patient'){
      this.showtextbox = false;
    } else if(selected === 'out_patient'){
      this.showtextbox = false;
    }
  }

  selectpathologistins(selectdins: any, pathinsti: any, pathfax: any, pathphone: any) {
    this.showtext = false;
    this.pathinstid = selectdins;
    this.patholoinsti = pathinsti;
    let fax = pathfax;
    let phone = pathphone;
    this.pathologistSelection.get('phone').setValue(phone);
    this.pathologistSelection.get('fax').setValue(fax);
    this.pathologistSelection.get('email').setValue(null);
    this.pathologistSelection.get('pathname').setValue('');
    this.pathologistSelection.get('othername').setValue('');
    if (pathinsti === 'Other') {
      this.showtextinsti = true;
      this.ReadOnly = false;
    } else {
      this.showtextinsti = false;
      this.ReadOnly = true;
    }
    this._SampleService.getpathologist(selectdins)
      .pipe(first())
      .subscribe((res: any) => {
        if (res) {
          this.pathologists = res.responseMessage;
          this.institutionID = res.responseMessage.length !== 0 ? res.responseMessage[0].pathologistInstitutionId : 0;
        } else {
          console.log('error');
        }
      });
  }


  pathologistdetails(selectppath: any, otherinstiname: any) {
    this.pathnameid = selectppath;
    this.pathologyname = otherinstiname;
    if (otherinstiname === 'Other') {
      this.showtext = true;
    } else {
      this.showtext = false;
    }
    if (this.institutionID) {
      this._SampleService.getpathologistdetails(this.institutionID, selectppath)
        .pipe(first())
        .subscribe((res: any) => {
          if (res) {
            this.pathologistdet = res.responseMessage[0];
            this.pathologistSelection.get('email').setValue(res.responseMessage[0].email);
          } else {
            console.log('error');
          }
        });
    } else {
      this._SampleService.getpathologistdetails(this.pathinsituteId, selectppath)
        .pipe(first())
        .subscribe((res: any) => {
          if (res) {
            this.pathologistdet = res.responseMessage[0];
            this.pathologistSelection.get('email').setValue(res.responseMessage[0].email);
          } else {
            console.log('error');
          }
        });
    }
  }

  closemessagebox() {
    this.messagebox = false;
    if (this.currentrole == 'Pathologist') {
      this.router.navigate(['/Jacksonlab/samplelist/All']);
    } else {
      this.router.navigate(['/Jacksonlab/samplelist/MySample']);
    }
    this.buttonDisabled = false;
    // $('.modal').modal('hide');
  }

  sampleSubmitlims() {
    this.isError = false;

    if (this.pathologyname === 'Other') {

      if (this.pathologistSelection.get('othername').value === '' || this.pathologistSelection.get('othername').value === null) {
        this.isError = true;
        this.othervalidshow = true;
      }
      else {
        this.othervalidshow = false;
      }
      if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
        this.isError = true;
        this.othervalidemail = true;
      }
      else {
        this.othervalidemail = false;
      }
    }
        if (this.patholoinsti == 'Other') {
          if (this.pathologistSelection.get('otherinstiname').value === '' || this.pathologistSelection.get('otherinstiname').value === null) {
            this.isError = true;
            this.otherinstivalidshow = true;
          }
          else {
            this.otherinstivalidshow = false;
          }
          if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
            this.isError = true;
            this.otherinstifax = true;
          }
          else {
            this.otherinstifax = false;
          }
        }

        if (!this.isError) {
          this.otherinstivalidshow = false;
          this.othervalidshow = false;
          this.othervalidemail = false;
          this.otherinstifax = false;
          this.loader = true;
          this.assignpathmsg = false;
          this._SampleService.previewsubmit(this.sampleId, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
            .subscribe((result: any) => {
              if (!result.isError) {
                
                this.loader = false;
                $('#sampleSubmitPreview').modal('hide');
                this.messagebox = true;
                this.messagecontent = 'Sample Submitted Successfully !';
              }
              else {
                this.loader = false;
                this.assignpathmsg = true;
                this.assignpatherror = result.errorMessage;
              }
            });
          }
      }
  
  emailvalidation() {
    if (!this.pathologistSelection.controls.email.valid) {
      this.emailbtn = true;
    }
    else {
      this.emailbtn = false;
    }
  }
  
  pathologyassign() {
    this.isError = false;
    if (this.pathologyname === 'Other') {

      if (this.pathologistSelection.get('othername').value === '') {
        this.isError = true;
        this.othervalidshow = true;
      }
      else {
        this.othervalidshow = false;
      }
      if (this.pathologistSelection.get('email').value === '' || this.pathologistSelection.get('email').value === null) {
        this.isError = true;
        this.othervalidemail = true;
      }
      else {
        this.othervalidemail = false;
      }
    }
        if (this.patholoinsti == 'Other') {
          if (this.pathologistSelection.get('otherinstiname').value === '') {
            this.isError = true;
            this.otherinstivalidshow = true;
          }
          else {
            this.otherinstivalidshow = false;
          }
          if (this.pathologistSelection.get('fax').value === '' || this.pathologistSelection.get('fax').value === null) {
            this.isError = true;
            this.otherinstifax = true;
          }
          else {
            this.otherinstifax = false;
          }
    }

    if (!this.isError) {
      this.otherinstivalidshow = false;
      this.othervalidshow = false;
      this.othervalidemail = false;
      this.otherinstifax = false;
      this.loader = true;
      this.assignpathmsg = false;
      this._SampleService.assignpath(this.returnsample, this.pathologistSelection.value, this.pathologyname, this.patholoinsti)
        .subscribe((result: any) => {
          if (!result.isError) {
            this.loader = false;
            $('#sampleSubmitPreview').modal('hide');
            this.messagebox = true;
            this.messagecontent = 'Sample Submitted Successfully !';
          }
          else {
            this.loader = false;
            this.assignpathmsg = true;
            this.assignpatherror = result.errorMessage;
          }
        });
      }
    }
     

  editsample(editsampleid) {
    this.buttonDisabled = false;
    if (this.currentrole === 'Pathologist') {
      this.stepper.selectedIndex = 2;
      this.router.navigate(['/Jacksonlab/editsample', editsampleid]);
    } else {
      this.stepper.selectedIndex = 0;
      this.router.navigate(['/Jacksonlab/editsample', editsampleid]);
    }
   
  }
}
