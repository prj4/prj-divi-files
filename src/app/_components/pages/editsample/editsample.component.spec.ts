import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsampleComponent } from './editsample.component';

describe('EditsampleComponent', () => {
  let component: EditsampleComponent;
  let fixture: ComponentFixture<EditsampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
