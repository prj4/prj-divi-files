import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { AuthenticationService } from '../../../../_services/authentication.service';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { SampleService } from '../../../../_services/sample.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string;
  currentrole: string;
  contacts: string;
  email: string;
  phone: any;

  constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private _SampleService: SampleService,
    private router: Router, private adalSvc: MsAdalAngular6Service, ) { }

  ngOnInit() {
    
    // this.username = this.adalSvc.LoggedInUserName;

    // this._SampleService.dashcontact()
    //   .subscribe((res: any) => {
    //     this.email = res.responseMessage.email;
    //     this.phone = res.responseMessage.phone;
    //   });
  }


  get user(): any {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  logOut() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.clear();
    this.adalSvc.logout();
  }

  

}
