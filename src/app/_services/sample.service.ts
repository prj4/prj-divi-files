import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environment';
import { Sampledata } from '../_models/sample';
import { Pathologistdata } from '../_models/pathologist';
import { ReviewDetails } from '../_models/pathologist';


@Injectable({ providedIn: 'root' })

export class SampleService {
  UserId: string;
  SampleID: any;


  constructor(private http: HttpClient) { }

  createSample(
    patientsampledetails: Sampledata,
    physampledetails: Sampledata,
    specsampledetails: Sampledata,
    billpaymenttype: Sampledata,
    billsampledetails: Sampledata,
    spepanel) {
    const specimenArray = [];
    for (let i = 0; i < spepanel.length; i++) {
      specimenArray.push
        ({
          'panelId': spepanel[i]
        });
    }

    const Samples = {
      'sampleId': 0,
      'patient': {
        'patientName': patientsampledetails.patname,
        'dob': patientsampledetails.patdob,
        'sex': patientsampledetails.patgender,
        'address': patientsampledetails.pataddress,
        'city': patientsampledetails.patcity,
        'state': patientsampledetails.patstate,
        'zipcode': patientsampledetails.patzcode,
        'country': patientsampledetails.patcountry,
        'phoneNumber': patientsampledetails.patphone,
        'medicalRecord': patientsampledetails.patmrecord
      },
      'specimen': {
        'specimenId': specsampledetails.specimenID,
        'collectedDate': specsampledetails.datetimecollected,
        'diagnosis': specsampledetails.diagnosis,
        'specimenSite': specsampledetails.specimensite,
        'storageRemovedDate': specsampledetails.daterfromstorage,
        'primarySpecimenSite': specsampledetails.pspecimensite
      },
      'bill': {
        'companyName': billsampledetails.billingcname,
        'memberId': billsampledetails.billingmemberid,
        'group': billsampledetails.billinggroup,
        'insuredName': billsampledetails.billinginsuredname,
        'city': billsampledetails.billingcity,
        'state': billsampledetails.billingstate,
        'dob': billsampledetails.billingdob,
        'phone': billsampledetails.billingphone,
        'studyId': billsampledetails.billinginsstudyname,
        'address': billsampledetails.billingaddressstudy,
        'patientStatus': billsampledetails.billingpstatus,
        'zipcode': billsampledetails.billingzcode,
        'RelationToPatientId': billsampledetails.relatopatient,
        'PatientStatusId': billsampledetails.patstatus,
        'dischargeDate': billsampledetails.dischargedate,
        'PaymentModeId': billpaymenttype.paymentmode
      },
      'specimenPanel': specimenArray,
      'physician': {
        'physicianId': physampledetails.phyname,
        'institutionId': physampledetails.phyinstution,
      }
    };
    return this.http.post(`${environment.apiUrl}Sample/CreateSample`, Samples);
  }

  updateSample(
    patientsampledetails: Sampledata,
    physampledetails: Sampledata,
    specsampledetails: Sampledata,
    billpaymenttype: Sampledata,
    billsampledetails: Sampledata,
    spepanel,
    sampleId) {
    
    const specimenArray = [];
    for (let i = 0; i < spepanel.length; i++) {
      specimenArray.push
        ({
          'panelId': spepanel[i]
        });
    }

    const Samples = {
      'sampleId': sampleId,
      'patient': {
        'patientName': patientsampledetails.patname,
        'dob': patientsampledetails.patdob,
        'sex': patientsampledetails.patgender,
        'address': patientsampledetails.pataddress,
        'city': patientsampledetails.patcity,
        'state': patientsampledetails.patstate,
        'zipcode': patientsampledetails.patzcode,
        'country': patientsampledetails.patcountry,
        'phoneNumber': patientsampledetails.patphone,
        'medicalRecord': patientsampledetails.patmrecord
      },
      'specimen': {
        'specimenId': specsampledetails.specimenID,
        'collectedDate': specsampledetails.datetimecollected,
        'diagnosis': specsampledetails.diagnosis,
        'specimenSite': specsampledetails.specimensite,
        'storageRemovedDate': specsampledetails.daterfromstorage,
        'primarySpecimenSite': specsampledetails.pspecimensite
      },
      'bill': {
        'companyName': billsampledetails.billingcname,
        'memberId': billsampledetails.billingmemberid,
        'group': billsampledetails.billinggroup,
        'insuredName': billsampledetails.billinginsuredname,
        'city': billsampledetails.billingcity,
        'state': billsampledetails.billingstate,
        'dob': billsampledetails.billingdob,
        'phone': billsampledetails.billingphone,
        'studyId': billsampledetails.billinginsstudyname,
        'address': billsampledetails.billingaddressstudy,
        'patientStatus': billsampledetails.billingpstatus,
        'zipcode': billsampledetails.billingzcode,
        'RelationToPatientId': billsampledetails.relatopatient,
        'PatientStatusId': billsampledetails.patstatus,
        'dischargeDate': billsampledetails.dischargedate,
        'PaymentModeId': billpaymenttype.paymentmode
      },
      'specimenPanel': specimenArray,
      'physician': {
        'physicianId': physampledetails.phyname,
        'institutionId': physampledetails.phyinstution,
      }
    };
    return this.http.put(`${environment.apiUrl}Sample/UpdateSample`, Samples);
  }

  assignpathologist(samplID, pathologist: Pathologistdata, pathothername, instituname) {
    const Pathologistselection = {
      'SampleId': samplID,
      'PathologistId': pathologist.pathname,
      'PathologistInstitutionId': pathologist.pathinstution,
      'pathologistName': pathothername,
      'otherPathologistName': pathologist.othername,
      "otherPathologistInstitutionName": pathologist.otherinstiname,
      'pathologistInstitution': instituname,
      'phone': pathologist.phone,
      'email': pathologist.email,
      'fax': pathologist.fax

    };
    return this.http.post(`${environment.apiUrl}Sample/SubmitSample`, Pathologistselection);
  }


  specimenpanel() {
    return this.http.get(`${environment.apiUrl}Institution/GetPanels`);
  }

  getinstitution() {
    return this.http.get(`${environment.apiUrl}Institution/GetPhysicianInstitution`);
  }

  getdoctors(instituteid, doctorid) {
    return this.http.get(`${environment.apiUrl}Institution/GetPhysician?InstitutionId=` + instituteid + `&PhysicianId=` + doctorid);
  }

  getdoctorsdetails(instituteid, doctorid) {
    return this.http.get(`${environment.apiUrl}Institution/GetPhysician?InstitutionId=` + instituteid + `&PhysicianId=` + doctorid);
  }

  getsamples() {
    return this.http.get(`${environment.apiUrl}Sample/GetSamplesList`);
  }

//   getsamplesoe(pn: any, ps: any) {
//   if (ps >= 1) {
//    return this.http.get(`${environment.apiUrl}specimen/GetByPage?PageNumber=` + pn + `&PageSize=10`);
//    } else {
//   return this.http.get(`${environment.apiUrl}specimen/GetByPage?PageNumber=` + pn + `&PageSize=10`);
//   }
// }

  getuser(id: any) {
    return this.http.get(`${environment.apiUrl}Sample/GetSample?SampleId=` + id);
  }

  deletesample(id: any) {
    return this.http.delete(`${environment.apiUrl}Sample/DeleteSample?SampleId=` + id);
  }

  getpathologistinstitution() {
    return this.http.get(`${environment.apiUrl}Institution/GetPathologistInstitution`);
  }

  getpathologist(pathid) {
    return this.http.get(`${environment.apiUrl}Institution/GetPathologist?InstitutionId=` + pathid + `&PathologistId=0` );
  }

  getpathologistdetails(instituteid, pathdetid) {
    return this.http.get(`${environment.apiUrl}Institution/GetPathologist?InstitutionId=` + instituteid + `&PathologistId=` + pathdetid);
  }

  getdashboard() {
    return this.http.get(`${environment.apiUrl}Dashboard/Dashboard`);
  }

  getpathreviewcontent() {
    return this.http.get(`${environment.apiUrl}Institution/GetSpecimenFeature`);
  }

  previewsubmit(id, pathologist: Pathologistdata, pathothername, instituname) {
    const Pathologistselection = {
      'SampleId': id,
      'PathologistId': pathologist.pathname,
      'PathologistInstitutionId': pathologist.pathinstution,
      "pathologistName": pathothername,
      'otherPathologistName': pathologist.othername,
      "otherPathologistInstitutionName": pathologist.otherinstiname,
      "pathologistInstitution": instituname,
      'phone': pathologist.phone,
      'email': pathologist.email,
      'fax': pathologist.fax
    };
    return this.http.post(`${environment.apiUrl}Sample/SubmitSample`, Pathologistselection);
  }

  samplebyfilter(value: any) {
    return this.http.get(`${environment.apiUrl}Sample/GetSamplesList?FilterBy=` + value, {});
  }

  reviewSubmit(cellcontent, spefeatures, sampleid, fileToUpload) {
    const formData: FormData = new FormData();
    const sfeturesArray = [];
      for (let i = 0; i < spefeatures.length; i++) {
        sfeturesArray.push
            ({
              'specimenFeatureId': spefeatures[i],
              'otherComment': cellcontent.specfeaturesother
            });
      }

    if (fileToUpload != null) {
      for (let i = 0; i < fileToUpload.length; i++) {
        formData.append('reviewUpload', fileToUpload[i], fileToUpload[i].name);
      }
    }
      formData.append('sampleId', sampleid);
      formData.append('neoplasticCellOne', cellcontent.cellcontentoptone);
      formData.append('neoplasticCellTwo', cellcontent.cellcontentopttwo);
      formData.append('specimenFeature', JSON.stringify(sfeturesArray));
    return this.http.post(`${environment.apiUrl}Sample/SubmitPathologistReview`, formData);

  }

  reviewSave(cellcontent, spefeatures, sampleid, fileToUpload) {
    const formData: FormData = new FormData();
    const sfeturesArray = [];
    for (let i = 0; i < spefeatures.length; i++) {
      sfeturesArray.push
        ({
          'specimenFeatureId': spefeatures[i],
          'otherComment': cellcontent.specfeaturesother
        });
    }

    if (fileToUpload != null) {
      for (let i = 0; i < fileToUpload.length; i++) {
        formData.append('reviewUpload', fileToUpload[i], fileToUpload[i].name);
      }
    }

    formData.append('sampleId', sampleid);
    formData.append('neoplasticCellOne', cellcontent.cellcontentoptone);
    formData.append('neoplasticCellTwo', cellcontent.cellcontentopttwo);
    formData.append('specimenFeature', JSON.stringify(sfeturesArray));
    return this.http.post(`${environment.apiUrl}Sample/SavePathologistReview`, formData);

  }

  downloadreview(id: any,filename) {
    return this.http.get(`${environment.apiUrl}File/DownloadReview?SampleId=` + id +`&FileName=` + filename, { responseType: 'blob'});
  }
  deletefile(id: any, filename) {
    return this.http.delete(`${environment.apiUrl}File/DeleteReviewFile?SampleId=` + id + `&FileName=` + filename);
  }

  getdashboardwizard(status: string) {
    return this.http.get(`${environment.apiUrl}Dashboard/DashboardList?FilterBy=` + status );
  }

  assignpath(id, pathologist: Pathologistdata, pathothername, instituname ) {
    const pathologistselection = {
      'sampleid': id,
      'pathologistid': pathologist.pathname,
      'pathologistinstitutionid': pathologist.pathinstution,
      "pathologistName": pathothername,
      "otherPathologistName": pathologist.othername,
      "otherPathologistInstitutionName": pathologist.otherinstiname,
      "pathologistInstitution": instituname,
      "phone": pathologist.phone,
      "email": pathologist.email,
      "fax": pathologist.fax
    };
    return this.http.post(`${environment.apiUrl}Sample/AssignSampleForOncologist`, pathologistselection);
  }

  printsample(id) {
    return this.http.get(`${environment.apiUrl}File/PrintSample?SampleId=` + id, { responseType: 'blob' });
  }

  getbillinstitute() {
    return this.http.get(`${environment.apiUrl}Institution/GetStudy`);
  }

  dashcontact() {
    return this.http.get(`${environment.apiUrl}Dashboard/Contact`);
  }

  digitalsign(id) {
    return this.http.get(`${environment.apiUrl}DocumentSign/Sign?SampleId=` +id);
  }

  studyemail(id) {
    return this.http.get(`${environment.apiUrl}DocumentSign/InitiateSignToOncologist?SampleId=` + id);
  }

  signresponse(id, signstatus){
    return this.http.post(`${environment.apiUrl}DocumentSign/SignResponse?SampleId=` + id + `&SignStatus=` + signstatus,{});
  }

  logtime() {
    return this.http.get(`${environment.apiUrl}Login/Login`);
  }

  signedsample(id) {
    return this.http.get(`${environment.apiUrl}DocumentSign/GetDocument?SampleId=` + id, { responseType: 'blob' });
  }


}
