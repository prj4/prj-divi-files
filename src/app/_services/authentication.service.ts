import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from "@angular/router"
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Observable, of, pipe, throwError } from 'rxjs';
import { map, switchMap, debounceTime, catchError } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public authtoken: any;

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  constructor(private http: HttpClient, private adalSvc: MsAdalAngular6Service, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<any>(sessionStorage.getItem('currentUser'));
    this.currentUser = this.currentUserSubject.asObservable();



    this.adalSvc.acquireToken('https://login.microsoftonline.com/').subscribe((resToken: string) => {

    

      if (resToken) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        sessionStorage.setItem('currentUser', resToken);
        this.currentUserSubject.next(resToken);
        
      }
    }),

      catchError(err => {

        alert('Invalid Credentials');
        return throwError(err);
      })
  }

}
