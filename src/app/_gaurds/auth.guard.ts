import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MsAdalAngular6Module, MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { AuthenticationGuard } from 'microsoft-adal-angular6';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  currentrole: string;
  constructor(
    private router: Router,
    private adalSvc: MsAdalAngular6Service
  ) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //const currentUser = sessionStorage.getItem('currentUser');

    // if (currentUser) {
    //   // authorised so return true
    //   return true;
    // }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
