import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { Material } from './Material';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import {
  MatMomentDateModule,
  MomentDateAdapter,
  MAT_MOMENT_DATE_FORMATS,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from '@angular/material-moment-adapter';
import { NgxPrintModule } from 'ngx-print';
import { AppComponent } from './app.component';
import { DashboardComponent } from './_components/pages/dashboard/dashboard.component';
import { SamplelistComponent } from './_components/pages/samplelist/samplelist.component';
import { SamplesubmissionComponent } from './_components/pages/samplesubmission/samplesubmission.component';
import { LoginComponent } from './_components/auth/login/login.component';
import { MsAdalAngular6Module, AuthenticationGuard } from 'microsoft-adal-angular6';
import { AzuComponent } from './_components/pages/azu/azu.component';
import { HeaderComponent } from './_components/layout/page_layout/header/header.component';
import { ContentComponent } from './_components/layout/page_layout/content/content.component';
import { MainLayoutComponent } from './_components/layout/main-layout/main-layout.component';

import { AuthGuard } from './_gaurds/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewComponent } from './_components/pages/view/view.component';
import { EditsampleComponent } from './_components/pages/editsample/editsample.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SamplelistComponent,
    SamplesubmissionComponent,
    LoginComponent,
    AzuComponent,
    ContentComponent,
    MainLayoutComponent,
    HeaderComponent,
    ViewComponent,
    EditsampleComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Material,
    NgxPrintModule,
    MsAdalAngular6Module.forRoot({
      tenant: '8789cd3a-74b6-41ae-8992-8be37e5f6bd1',
      clientId: '87272d4a-cf02-4145-bb6b-05f9d053665a',
      redirectUri: window.location.origin,
      endpoints: {
        "http://localhost:4200/": "abfbad9d-1bcc-45b7-8d10-9ba188f97a43",
      },
      navigateToLoginRequestUrl: false,
      cacheLocation: 'sessionStorage',
    }),
    RouterModule.forRoot([
      { path: '', component: LoginComponent},
      { path: 'azu', component: AzuComponent, canActivate: [AuthenticationGuard]},
      {
        path: 'Jacksonlab',
        component: ContentComponent,
        children:
          [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'samplelist', component: SamplelistComponent, canActivate: [AuthGuard] },
            { path: 'samplelist/:countId', component: SamplelistComponent, canActivate: [AuthGuard] },
            { path: 'samplesubmission', component: SamplesubmissionComponent, canActivate: [AuthGuard] },
            { path: 'viewsample/:sampleId', component: ViewComponent, canActivate: [AuthGuard] },
            { path: 'editsample/:sampleId', component: EditsampleComponent, canActivate: [AuthGuard] },
          ]
      },

    ]),
     BrowserAnimationsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },

    AuthenticationGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
